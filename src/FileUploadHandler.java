
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 * Servlet implementation class FileUploadHandler
 */
@WebServlet("/FileUploadHandler")
public class FileUploadHandler extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public FileUploadHandler() {
		super();
		// TODO Auto-generated constructor stub
	}

	// Location of the folder
	private static final String UPLOAD_DIRECTORY = "E:/Work/Projects/Workspace/HOBSAnalyticVisu/WebContent/Uploads";

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String res = "", newFilename = "";
		String formattedDate;
		LocalDateTime myDateObj = LocalDateTime.now();
		DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");

		PrintWriter writer = response.getWriter();

		// Checks if the form has 'enctype=multipart/form-data' attribute in it.
		if (!ServletFileUpload.isMultipartContent(request)) {
			writer.append("Form tag must has  'enctype=multipart/form-data' attribute");
		} else {
			// Creates the directory if it does not exist in the specified location
			File uploadDir = new File(UPLOAD_DIRECTORY);
			if (!uploadDir.exists()) {
				uploadDir.mkdir();
			}

			try {
				List<FileItem> multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
				for (FileItem item : multiparts) {
					if (!item.isFormField()) {
						String name = new File(item.getName()).getName();
						
						myDateObj = LocalDateTime.now();
					
						formattedDate= myDateObj.format(myFormatObj);
						formattedDate = formattedDate.replace('-', '_');
						formattedDate = formattedDate.replace(' ', '_');
						formattedDate = formattedDate.replace(':', '_');
						
						name = formattedDate + "_" + name;
						
						item.write(new File(UPLOAD_DIRECTORY + File.separator + name));
						
						res = name;
					}
				}
				writer.append("success,");
				writer.append(Math.abs(res.hashCode()) + ",");
				writer.append(res+ ",");
			} catch (Exception e) {
				writer.append("error occured : " + e.getMessage());

			}
		}

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}
}
