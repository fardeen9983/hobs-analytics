package com.tcs.analytic.rest;

import javax.ws.rs.core.*;

import com.tcs.analytic.service.FileDetailService;
import com.tcs.analytic.service.impl.FileDetailServiceImpl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import javax.ws.rs.*;


@Path("/file")
public class FileDetailRestService {

	FileDetailService f1=new FileDetailServiceImpl();
	@POST
	@Path("/setFile/{filename}/{coldel}/{rowdel}/{header}")
	public List<Object> setFileDetail(
			@PathParam("filename") String fname,
			@PathParam("coldel") String coldel,
			@PathParam("rowdel") String rowdel,
			@PathParam("header") String header)
	{
		return f1.setFileData(fname, coldel, rowdel, header);
	}
	
	
	
	
}

