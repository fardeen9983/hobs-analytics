package com.tcs.analytic.rest;

import java.io.*;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

@Path("/workspace")
public class WorkspaceRestService {

	private Gson gson = new Gson();
	private final String PATH = "C:/HOB_workspace/";

	@Path("/getDir")
	@POST
	@Produces(MediaType.TEXT_HTML)
	public String getJSON() throws IOException {

//		String path =form.asMap().get("path").toString();
//		path.replaceAll(,"/");
//		String path = getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
//		String path = System.getProperty("")
		File dir = new File(PATH);

		File[] files = dir.listFiles();
		FileFilter fileFilter = new FileFilter() {
			public boolean accept(File file) {
				return file.isDirectory();
			}
		};
		files = dir.listFiles(fileFilter);
		String res = "";
		for (File x : files) {

			res += x.getName() + ",";
		}

		return res;
	}

	@Path("/save")
	@POST
	@Produces(MediaType.TEXT_HTML)
	public String saveWorkspace(String data) {
		System.out.println(data);
		JsonObject obj = gson.fromJson(data, JsonObject.class);
		String workspace = obj.get("workspace").getAsString(), name = obj.get("name").getAsString(),
				workspaceName = obj.get("workspaceName").getAsString();
		String json = obj.get("json").getAsString();
		System.out.print(json);

		if (workspace.equals("current_workspace")) {
			String DIR = PATH + "current";
			File dirFile = new File(DIR);
			if (!dirFile.exists()) {
				dirFile.mkdir();
			}

			File myObj = new File(DIR + "/" + name + ".json");

			// try-with-resources statement based on post comment below :)
			try {
				FileWriter file = new FileWriter(DIR + "/" + name + ".json");
				if (myObj.createNewFile()) {
					System.out.println("File created: " + myObj.getName());
				}
				file.write(json.toString());
				System.out.println("Successfully Copied JSON Object to File...");
				System.out.println("\nJSON Object: " + json);
				file.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else if (workspace.equals("new_workspace")) {

			String UPLOAD_DIRECTORY = PATH + workspaceName;

			File uploadDir = new File(UPLOAD_DIRECTORY);
			if (!uploadDir.exists()) {
				uploadDir.mkdir();
			}
			// out.println(UPLOAD_DIRECTORY);
			File myObj = new File(UPLOAD_DIRECTORY + "/" + name + ".json");
			try {
				if (myObj.createNewFile()) {
					System.out.println("File created: " + myObj.getName());
				}
				FileWriter file = new FileWriter(UPLOAD_DIRECTORY + "/" + name + ".json");
				file.write(json.toString());
				System.out.println("Successfully Copied JSON Object to File...");
				System.out.println("\nJSON Object: " + json);
				file.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		return "Saved";
	}
}
