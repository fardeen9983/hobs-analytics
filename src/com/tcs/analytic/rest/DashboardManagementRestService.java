package com.tcs.analytic.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.tcs.analytic.service.DashboardManagementService;
import com.tcs.analytic.service.impl.DashboardManagementServiceImpl;

@Path("/dashboard")
public class DashboardManagementRestService {
	
	DashboardManagementService dashboardManagementService = new DashboardManagementServiceImpl();
	
	@Path("/create/{name}/{details}")
	@Produces("application/json")
	@GET
	public List<Object> createDashboard(@PathParam("name") String name, @PathParam("details") String details)
	{
		//System.out.println("REST CALLED");
		return dashboardManagementService.createDashboard(name, details);
		
	}
	
	@Path("/update/{id}/{name}/{details}")
	@Produces("application/json")
	@GET
	public List<Object> createDashboard(@PathParam("id") int id, @PathParam("name") String name, @PathParam("details") String details)
	{
		//System.out.println("REST CALLED");
		return dashboardManagementService.updateDashboard(id,name, details);
		
	}
	
	@Path("/get/{id}")
	@Produces("application/json")
	@GET
	public List<Object> getDashboard(@PathParam("id") int id)
	{
		//System.out.println("REST CALLED ===============================================");
		return dashboardManagementService.getDashboard(id);
		
	}
	
	@Path("/list")
	@Produces("application/json")
	@GET
	public List<Object> getDashboardList()
	{
		//System.out.println("REST CALLED ===============================================");
		return dashboardManagementService.getDashboardList();
		
	}
	
	@Path("/workspaces/get")
	@Produces("application/json")
	@GET
	public List<Object> getWorkspaces()
	{
		//System.out.println("REST CALLED");
		return dashboardManagementService.getWorkspaces();
		
	}
	
	@Path("/charts/get/{workspace}")
	@Produces("application/json")
	@GET
	public List<Object> getChartList(@PathParam("workspace") String workspace)
	{
		//System.out.println("REST CALLED");
		return dashboardManagementService.getChartList(workspace);
		
	}

}
