package com.tcs.analytic.rest;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.tcs.analytic.eo.DbConfig;
import com.tcs.analytic.service.OtableService;
import com.tcs.analytic.service.impl.OtableServiceImpl;

import java.io.File;
import java.util.*;

@Path("/tables")
public class OtableRestService {

	private static final String UPLOAD_DIRECTORY = "E:/ECLIPSE-WORKSPACE/HOBSAnalyticVisu/WebContent/Uploads";

	OtableService obj = new OtableServiceImpl();

	@Path("/getOTableList")
	@Produces("application/json")
	@GET
	public List<Object> getOTableList() {
		return obj.getOTableList();
	}

	@Path("/getMTableList")
	@Produces("application/json")
	@GET
	public List<Object> getMTableList() {
		return obj.getMTableList();
	}

	@Path("/getDbConfig")
	@Produces("application/json")
	@GET
	public List<DbConfig> getList() {
		return obj.getDb();
	}

	@Path("/deleteFile/{filename}")
	@Produces("application/json")
	@POST
	public List<Object> removeFile(@PathParam("filename") String fname) {
		List<Object> lst = new ArrayList<>();

		String res = "";
		try {
			File f = new File(UPLOAD_DIRECTORY + File.separator + fname);
			if (f.delete()) {

				res = "success";
				lst.add(res);
				// return Response.status(200).entity("success").build();
			} else {

				res = "error";
				lst.add(res);
			}
		} catch (Exception e) {

			res = "error";
			lst.add(res);
		} finally {

		}

		// if(res == )
		return lst;
	}

}
