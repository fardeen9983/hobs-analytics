package com.tcs.analytic.rest;

import java.io.*;
import java.util.Properties;

public class ConfigReader {
	InputStream input;
	String result;
	public String getProperty(String property) throws IOException {
		String propFileName = "config.properties";
		try {
			Properties prop = new Properties();
			input = getClass().getResourceAsStream(propFileName);
			if (input != null) {
				prop.load(input);
			} else {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			}
			result = prop.getProperty(property);
			System.out.println(result);
		} catch (Exception e) {

		}
		finally {
//			input.close();
		}
		return result;
	}
}
