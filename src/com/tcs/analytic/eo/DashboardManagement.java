package com.tcs.analytic.eo;

public class DashboardManagement {
	int id;
	String dashboardName, dashboardData;
	
	public DashboardManagement() {
		super();
	}
	
	public DashboardManagement(int id) {
		super();
		this.id = id;
	}
	
	public DashboardManagement(int id, String dashboardName, String dashboardData) {
		super();
		this.id = id;
		this.dashboardName = dashboardName;
		this.dashboardData = dashboardData;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDashboardName() {
		return dashboardName;
	}
	public void setDashboardName(String dashboardName) {
		this.dashboardName = dashboardName;
	}
	public String getDashboardData() {
		return dashboardData;
	}
	public void setDashboardData(String dashboardData) {
		this.dashboardData = dashboardData;
	}
	
	
	

}
