package com.tcs.analytic.eo;

import java.util.HashMap;


import javax.json.JsonObject;

public class ChartConfig {
	private String name;
	private HashMap<String,Double> size;
	private HashMap<String,String> labels;
	private String bindTo;
	private String[] keys;
	private JsonObject json;
}
