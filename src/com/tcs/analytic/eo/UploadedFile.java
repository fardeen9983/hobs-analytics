package com.tcs.analytic.eo;

public class UploadedFile {

	private int fid;
	private String filename;
	private String coldel;
	private String rowdel;
	private int hindex;

	public UploadedFile() {

	}

	public UploadedFile(int fid, String filename, String coldel, String rowdel, int hindex) {
		super();
		this.fid = fid;
		this.filename = filename;
		this.coldel = coldel;
		this.rowdel = rowdel;
		this.hindex = hindex;
	}

	public int getFid() {
		return fid;
	}

	public void setFid(int fid) {
		this.fid = fid;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getColdel() {
		return coldel;
	}

	public void setColdel(String coldel) {
		this.coldel = coldel;
	}

	public String getRowdel() {
		return rowdel;
	}

	public void setRowdel(String rowdel) {
		this.rowdel = rowdel;
	}

	public int getHindex() {
		return hindex;
	}

	public void setHindex(int hindex) {
		this.hindex = hindex;
	}

}
