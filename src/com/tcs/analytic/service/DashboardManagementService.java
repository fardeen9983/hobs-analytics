package com.tcs.analytic.service;

import java.util.List;

public interface DashboardManagementService {

	public List<Object> createDashboard(String name, String details);
	
	public List<Object> updateDashboard(int id, String name, String details);
	
	public List<Object> getDashboard(int id);
	
	public List<Object> getDashboardList();
	
	public List<Object> getWorkspaces();
	
	public List<Object> getChartList(String workspace);
}


