package com.tcs.analytic.service.impl;

import java.util.List;

import com.tcs.analytic.dao.DashboardManagementDao;
import com.tcs.analytic.dao.impl.DashboardManagementDaoImpl;
import com.tcs.analytic.service.DashboardManagementService;

public class DashboardManagementServiceImpl implements DashboardManagementService {

	DashboardManagementDao dashboardManagementDao = new DashboardManagementDaoImpl();
	
	@Override
	public List<Object> createDashboard(String name, String details) {
	
		return dashboardManagementDao.createDashboard(name, details);
	
	}
	
	@Override
	public List<Object> updateDashboard(int id,String name, String details) {
	
		return dashboardManagementDao.updateDashboard(id, name, details);
	
	}
	
	@Override
	public List<Object> getDashboard(int id) {
	
		return dashboardManagementDao.getDashboard(id);
	
	}
	
	@Override
	public List<Object> getDashboardList() {
	
		return dashboardManagementDao.getDashboardList();
	
	}
	
	@Override
	public List<Object> getWorkspaces() {
	
		return dashboardManagementDao.getWorkspaces();
	
	}
	
	@Override
	public List<Object> getChartList(String workspace) {
	
		return dashboardManagementDao.getChartList(workspace);
	
	}

}
