package com.tcs.analytic.service.impl;
import java.util.ArrayList;
import java.util.List;
import com.tcs.analytic.dao.impl.*;
import com.tcs.analytic.eo.DbConfig;
import com.tcs.analytic.dao.*;
import com.tcs.analytic.service.*;
@SuppressWarnings("unused")
public class OtableServiceImpl implements OtableService {
	
	OtableDao obj=new OtableDaoImpl();
	
	public List<Object> getOTableList()
	{
		
		return obj.getDataOracle();
		
	}
	public List<Object> getMTableList()
	{
		
		return obj.getDataMysql();
		
	}
	public List<DbConfig> getDb()
	{
		return obj.getDbConfig();
	}
}
