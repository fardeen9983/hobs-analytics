package com.tcs.analytic.service;
import java.util.*;
import com.tcs.analytic.eo.*;
public interface OtableService {

	public List<Object> getOTableList();
	public List<Object> getMTableList();
	public List<DbConfig> getDb();
}
