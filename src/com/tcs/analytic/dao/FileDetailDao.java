package com.tcs.analytic.dao;

import java.util.List;

public interface FileDetailDao {

	public List<Object> setFileDetail(String fname, String coldel, String rowdel, String headerindex);

}
