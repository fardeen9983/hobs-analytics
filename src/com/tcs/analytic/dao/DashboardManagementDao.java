package com.tcs.analytic.dao;

import java.util.List;

public interface DashboardManagementDao {

	public List<Object> createDashboard(String name,String details);
	
	public List<Object> updateDashboard(int id, String name,String details);
	
	public List<Object> getWorkspaces();
	
	public List<Object> getChartList(String workspace);
	
	public List<Object> getDashboard(int id);
	
	public List<Object> getDashboardList();
	
}
