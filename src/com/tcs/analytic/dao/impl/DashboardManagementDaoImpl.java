package com.tcs.analytic.dao.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.mysql.cj.xdevapi.JsonArray;
import com.tcs.analytic.dao.DashboardManagementDao;
import com.tcs.analytic.eo.DashboardManagement;
import com.tcs.analytic.eo.DbConfig;

import org.json.JSONObject;
import org.json.JSONArray;

public class DashboardManagementDaoImpl implements DashboardManagementDao {

	@Override
	public List<Object> createDashboard(String name, String details) {
		List<Object> obj = new ArrayList<Object>();

		Configuration configuration = new Configuration().configure("hibernate.cfg.xml");

		SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		DashboardManagement dashboardManagement = new DashboardManagement();
		dashboardManagement.setDashboardName(name);
		dashboardManagement.setDashboardData(details);

		obj.add(session.save(dashboardManagement));

		transaction.commit();
		session.close();
		return obj;
	}

	@Override
	public List<Object> updateDashboard(int id, String name, String details) {
		List<Object> obj = new ArrayList<Object>();

		Configuration configuration = new Configuration().configure("hibernate.cfg.xml");

		SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		try {
			DashboardManagement dashboardManagement = new DashboardManagement();
			dashboardManagement.setId(id);
			dashboardManagement.setDashboardName(name);
			dashboardManagement.setDashboardData(details);

			session.update(dashboardManagement);

			transaction.commit();
			obj.add("success");
		} catch (Exception ex) {
			if (transaction != null) {
				transaction.rollback();
				obj.add("error");
			}
		} finally {
			session.close();
		}
		return obj;
	}

	@Override
	public List<Object> getWorkspaces() {
		String path = "E:/ECLIPSE-WORKSPACE/HOBSAnalyticVisu/WebContent/Uploads/workspaces";
		List<Object> obj = new ArrayList<Object>();

		File folder = new File(path);
		File[] listOfFiles;

		try {
			listOfFiles = folder.listFiles();
			if (listOfFiles != null && listOfFiles.length > 0) {
				obj.add("default");
				for (File file : listOfFiles) {
					if (file.isDirectory()) {
						obj.add(file.getName());
					}
				}
				obj.add(0, "success");
			} else {
				obj.add(0, "empty");
			}

		} catch (Exception ex) {
			obj.add(0, "error");
		} finally {

		}
		return obj;
	}

	@Override
	public List<Object> getChartList(String workspace) {
		String path = "E:/ECLIPSE-WORKSPACE/HOBSAnalyticVisu/WebContent/Uploads/workspaces";
		List<Object> obj = new ArrayList<Object>();

		if (!workspace.trim().equalsIgnoreCase("default")) {
			path = path + File.separator + workspace;
		}
		File folder = new File(path);
		File[] listOfFiles;
		String chartList = "[";
		String currentFile = "";
		JSONObject jarray = new JSONObject();
		JSONObject jobj = new JSONObject();

		if (folder != null && folder.exists()) {
			try {
				listOfFiles = folder.listFiles();
				if (listOfFiles != null && hasFiles(path)) {
					for (File file : listOfFiles) {
						if (file.isFile()) {
							currentFile = workspace + " " + file.getName();
							/*
							 * chartList += "{'" + Math.abs(currentFile.hashCode()) + "' :[ 'file': '"+
							 * file.getName() + "', 'workspace': '"+workspace+"' ] },";
							 */
							JSONObject jo = new JSONObject();
							jo.put("file", file.getName());
							jo.put("workspace", workspace);
							jo.put("hash", Math.abs(currentFile.hashCode()) + "");
							JSONArray ja = new JSONArray();
							ja.put(jo);

							jarray.put(Math.abs(currentFile.hashCode()) + "", ja);
						}
					}
					obj.add(0, "success");
					chartList += "]";
					// obj.add(chartList);
					jobj.put("charts", jarray);
					obj.add(jobj.toString());
				} else {
					obj.add(0, "empty");
					obj.add(1, workspace);
				}
			} catch (Exception ex) {
				obj.add(0, "error");
			} finally {
			}

		} else {
			obj.add(0, "not exist");
			obj.add(1, workspace);
			obj.add(2, folder.getName());
		}

		return obj;
	}
	// end of getChartList() ==========================================

	// function hasfiles() ==========================================
	boolean hasFiles(String folder) {
		int flag = 0;
		File f = new File(folder);
		File[] listOfFiles;

		listOfFiles = f.listFiles();
		if (listOfFiles != null && listOfFiles.length > 0) {
			for (File file : listOfFiles) {
				if (file.isFile()) {
					flag = 1;
				}
			}
		}
		if (flag == 1)
			return true;
		else
			return false;
	}
	// end of hasFiles() ==========================================

	@Override
	public List<Object> getDashboard(int id) {
		List<Object> obj = new ArrayList<Object>();

		Configuration configuration = new Configuration().configure("hibernate.cfg.xml");

		SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		try {
			DashboardManagement dashboard = (DashboardManagement) session.get(DashboardManagement.class, id);
			//System.out.println("=============================\nname : " + dashboard.getDashboardName() + "\n" + "data : " + dashboard.getDashboardData() + "======================");
			
			if (dashboard != null) {
				transaction.commit();
				obj.add(0, "success");
				//obj.add(dashboard);
				obj.add(dashboard.getId());
				obj.add(dashboard.getDashboardName());
				obj.add(dashboard.getDashboardData());
			} 
			else {
				transaction.commit();
				obj.add(0, "not found");
			}

		} catch (Exception ex) {
			if (transaction != null) {
				transaction.rollback();
				obj.add("exception");
				//obj.add(ex.getMessage());
			}
		} finally {
			session.close();
		}
		return obj;
	}
	
	
	@Override
	public List<Object> getDashboardList() {
		List<Object> obj = new ArrayList<Object>();

		Configuration configuration = new Configuration().configure("hibernate.cfg.xml");

		SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		try {
			List<DashboardManagement> obj1=session.createQuery("from DashboardManagement").list();
			 for (Iterator iterator = obj1.iterator(); iterator.hasNext();){
				
		            DashboardManagement dashboard = (DashboardManagement) iterator.next();
		            obj.add(dashboard);
		         }
			// obj.add(obj1);
			 obj.add(0,"success"); 
			 transaction.commit();
		} catch (Exception ex) {
			if (transaction != null) {
				transaction.rollback();
				obj.add(0,"exception");
				obj.add(ex.getMessage());
			}
		} finally {
			session.close();
		}
		return obj;
	}
} // end of class ==========================================
