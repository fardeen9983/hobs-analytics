package com.tcs.analytic.dao.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Projections;

import com.tcs.analytic.dao.FileDetailDao;
import com.tcs.analytic.eo.DbConfig;
import com.tcs.analytic.eo.UploadedFile;
import com.tcs.analytic.eo.*;

public class FileDetailDaoImpl implements FileDetailDao {

	public List<Object> setFileDetail(String fname, String coldel, String rowdel, String headerindex) {
		// String seperator = "~%7C~";
		String seperator = "~";
		List<Object> obj = new ArrayList<Object>();
		String filenames[] = fname.split(seperator);
		String coldels[] = coldel.split(seperator);
		String rowdels[] = rowdel.split(seperator);
		String headerindexs[] = headerindex.split(seperator);

		Configuration configuration = new Configuration().configure("hibernate.cfg.xml");

		SessionFactory sessionFactory = configuration.buildSessionFactory();

		Session session = sessionFactory.openSession();

		// 4. Starting Transaction
		Transaction transaction = session.beginTransaction();

		for (int i = 0; i < filenames.length; i++) {
			UploadedFile f1 = new UploadedFile();
			f1.setFilename(filenames[i]);
			f1.setColdel(coldels[i]);
			f1.setRowdel(rowdels[i]);
			f1.setHindex(Integer.parseInt(headerindexs[i]));
			obj.add(session.save(f1));

		}

		transaction.commit();
		session.close();
		return obj;

	}

}
