package com.tcs.analytic.dao.impl;
import com.tcs.analytic.dao.*;
import com.tcs.analytic.eo.*;
import java.util.*;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
public class OtableDaoImpl implements OtableDao {

	@SuppressWarnings({ "deprecation", "rawtypes" })
	@Override
	public List<Object> getDataOracle()
	{
		
		List<Object> obj=new ArrayList<Object>();
		
		
		try {
				
				
				Configuration configuration = new Configuration().configure("hibernate.cfg.xml");
	
				SessionFactory sessionFactory = configuration.buildSessionFactory();
	
				Session session = sessionFactory.openSession();
	
				// 4. Starting Transaction
				Transaction transaction = session.beginTransaction();
				List obj1=session.createSQLQuery("select table_name from user_tables").list();
				 for (Iterator iterator = obj1.iterator(); iterator.hasNext();){
			            Object table = (Object) iterator.next(); 
			            obj.add(table);
			         }
			         
				transaction.commit();
				session.close();
				
		} catch (HibernateException e) {
				System.out.println(e.getMessage());
				System.out.println("error");
		}
		return obj;
		
	}
	@SuppressWarnings({ "rawtypes", "deprecation" })
	@Override
	public List<Object> getDataMysql()
	{
		
		List<Object> obj=new ArrayList<Object>();
		
		
		try {
				
				
				Configuration configuration = new Configuration().configure("mhibernate.cfg.xml");
	
				SessionFactory sessionFactory = configuration.buildSessionFactory();
	
				Session session = sessionFactory.openSession();
	
				// 4. Starting Transaction
				Transaction transaction = session.beginTransaction();
				List obj1=session.createSQLQuery("select table_name from information_schema.tables where table_schema='analytic'").list();
				 for (Iterator iterator = obj1.iterator(); iterator.hasNext();){
			            Object table = (Object) iterator.next(); 
			            obj.add(table);
			         }
			         
				transaction.commit();
				session.close();
				
		} catch (HibernateException e) {
				System.out.println(e.getMessage());
				System.out.println("error");
		}

		
		
		
		
		return obj;
		
	}
	@SuppressWarnings("rawtypes")
	@Override
	public List<DbConfig> getDbConfig() {
		List<DbConfig> obj2=new ArrayList<DbConfig>();
		
		try {
			Configuration configuration = new Configuration().configure("hibernate.cfg.xml");

			SessionFactory sessionFactory = configuration.buildSessionFactory();

			Session session = sessionFactory.openSession();

			// 4. Starting Transaction
			Transaction transaction = session.beginTransaction();
			List obj1=session.createQuery("from DbConfig").list();
			 for (Iterator iterator = obj1.iterator(); iterator.hasNext();){
				
		            DbConfig employee = (DbConfig) iterator.next(); 
		            obj2.add(employee);
		         }
		         
			transaction.commit();
			session.close();
	
		} catch (HibernateException e) {
			System.out.println(e.getMessage());
			System.out.println("error");
		}
		return obj2;
	}


	
	
	
}
