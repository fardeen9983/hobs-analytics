package com.tcs.analytic.dao;
import java.util.*;
import com.tcs.analytic.eo.*;
public interface OtableDao {
	
	public List<Object> getDataOracle();
	public List<Object> getDataMysql();
	public List<DbConfig> getDbConfig();
	
}
