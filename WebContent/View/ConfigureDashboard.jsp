<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Configure Dashboard | HOBS Analytics and Visualization</title>

<link rel="stylesheet" href="../resources/css/bootstrapNEW.min.css" />
<link rel="stylesheet" href="../resources/css/w3.css" />
<link rel="stylesheet" href="../resources/css/jquery-ui.min.css" />
<link rel="stylesheet" href="../resources/css/loader.css" />
<link rel="stylesheet" href="../resources/css/customStyle1.css">
<link rel="stylesheet" href="../resources/css/gridstack.css" />
<link rel="stylesheet" href="../resources/css/sweetalert2.min.css" />
<link rel="stylesheet" href="../resources/css/css-loader.css" />

<script type="text/javascript" src="../resources/js/jquery-3.x.js"></script>
<script type="text/javascript"	src="../resources/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../resources/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="../resources/js/gridstack.js"></script>
<script type="text/javascript"	src="../resources/js/gridstack.jQueryUI.js"></script>
<script type="text/javascript"	src="../resources/js/sweetalert2.all.min.js"></script>

<!-- Adding C3.js css file -->
<link rel="stylesheet" href="../resources/css/c3.min.css" />
<!-- Adding C3.js and D3.js  file -->
<script type="text/javascript" src="../resources/js/d3.min.js"></script>
<script type="text/javascript" src="../resources/js/c3.min.js"></script>

<link rel="stylesheet" href="../resources/css/dashboard.css" />
<script type="text/javascript" src="../resources/js/dashboard.js"></script>

</head>
<body>

<div id="customLoader" class="loader loader-default is-active" data-text="Loading..."></div>
	<div class="main-wrapper">
	<input type="hidden" id="hf_dashboard_id" />
	<input type="hidden" id="hf_dashboard_state" />
	<input type="hidden" id="hf_dashboard_name" />
		<div class="header bg-dark-custom">
			<h3 class="w3-text-white">HOBS Data Analytics and Visualization</h3>
		</div>

		<!-- main container  ========================================================== -->
		<div class="main-content container">

			<br>
			<!-- first row ============================================ -->
			<div class="w3-row-padding">

				<div class="w3-col form-inline">
					<div class="form-group">
					<from id="the-form" >
						<label for="dashboard_name">Dashboard Name : </label> <input
							type="text" id="dashboard_name" name="dashboard_name"
							class="form-control error" /> <input type="button"
							class="btn btn-primary" value="Save"  onclick="saveData()" />
							 <input type="button"
							class="btn btn-success" value="Publish" />
							
							<input type="button"	class="btn btn-warning" value="Load Grid From JSON" onclick="loadGrid()"  />
							</from>
					</div>
				</div>
				<!-- <div class="w3-col m4 l4">
					<input type="button" class="btn btn-primary" value="Save" /> <input
						type="button" class="btn btn-success" value="Publish" />
				</div> -->
			</div>
			<!-- end of first row ========================================== -->
			<br>

			<div class="w3-row-padding" style="min-height:350px; width: 100%;">
				<div class="w3-col m8 l9" style="height: 100%;">
					<div id="#dashboard-container" class="dashboard-container"
						style="border: 2px solid black; height: 100%;">

						<div class="grid-stack" id="grid1" style="min-height:350px;" data-gs-animate="true">
							
						</div> 
					</div>
				</div>
				<div class="w3-col m4 l3" style="height: 100%;">
					<div  class="chart-list" style="border: 2px solid grey; height: 100%;min-height:350px">
							<label for="workspace_list">Workspace : </label>
							<select name="workspace_list" id="workspace_list" class="form-control" > <option> select Workspace </option> </select>
							<hr>
							<div class="grid-stack" id="grid2" style="min-height:100%;" data-gs-animate="true">
								<!-- <div id="list_chart1" class="chart1 grid-stack-item newChart "	data-gs-width="4" data-gs-height="1" data-gs-no-resize>
									<div class="grid-stack-item-content">chart1 fgjghjhgjytjuykuy </div>
								</div>
								<div  id="list_chart2" class="chart2 grid-stack-item newChart"  data-gs-width="4" data-gs-height="1" data-gs-no-resize>
									<div class="grid-stack-item-content">chart2</div>
								</div>
								<div  id="list_chart3" class="chart3 grid-stack-item newChart" data-gs-width="4" data-gs-height="1" data-gs-no-resize>
									<div class="grid-stack-item-content">chart3</div>
								</div>
								<div  id="list_chart4"  class="chart4 grid-stack-item newChart"  data-gs-width="4" data-gs-height="1" data-gs-no-resize>
									<div class="grid-stack-item-content">chart4</div>
								</div> -->
							<br>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- end of main container  ========================================================== -->
	</div>
	<br>
	<br>
	<br>

<script type="text/javascript">
/*================ fardeen's code ======================================================================= */

/*
 * Global chart variables
 */
var chart, columns, config = {type:""}, chartSelected = "";
var jsonData = [ {
	'data1' : 30,
	'data2' : 50
}, {
	'data1' : 200,
	'data2' : 20
}, {
	'data1' : 100,
	'data2' : 10
}, {
	'data1' : 400,
	'data2' : 40
}, {
	'data1' : 150,
	'data2' : 15
}, {
	'data1' : 250,
	'data2' : 25
} ];

/*
 * This function is used to generate configuration for chart
 * ====================
 */
function getChartConfig(type, eleId) {
	if (jsonData != null) {
		config['type'] = type;
		config['json'] = jsonData;
		config['keys'] = Object.keys(jsonData[0]);
		config['chart'] = null;
		config['bindTo'] = "#chart";
		config['size'] = {
			width : 200,
			height : 200
		};

		// Generate columns from data
		var temp = [];
		config.keys.forEach(function(e) {
			temp.push([ e ]);
		});

		for (var i = 0; i < jsonData.length; i++) {
			var data = jsonData[i];
			for (var j = 0; j < temp.length; j++) {
				temp[j].push(data[temp[j][0]]);
			}
		}
		config['cols'] = temp;

		// Call chart generation
		return generateChart(eleId);
	} else
		return false;
}

/*
 * This function is used to generate the chart based on config
 * ====================
 */
function generateChart(eleId) {
	if (config.hasOwnProperty('type')) {
		config.chart = c3.generate({
			//bindto : config.bindTo,
			bindto: "#" + eleId,
			data : {
				columns : config.cols,
				type : config.type
			},
			size : config.size
		});

		$(".chart-zone").css("border", "1px solid black");

		switch (config.type) {
		case "bar":
				
			break;
		case "line":
			
			break;
		case "pie":
			
			break;
		}
		$(".chart-options").css("border", "1px solid rgba(0, 0, 0, .1)");
		// switch (type) {
		// case "line":
		// chart = c3.generate({
		// bindto : '#chart',
		// data : {
		// columns : [ [ 'data1', 30, 200, 100, 400, 150, 250 ],
		// [ 'data2', 50, 20, 10, 40, 15, 25 ] ]
		// }
		// });
		// break;
		// case "bar":
		// chart = c3.generate({
		// bindto : '#chart',
		// data : {
		// columns : [ [ 'data1', 30, 200, 100, 400, 150, 250 ],
		// [ 'data2', 50, 20, 10, 40, 15, 25 ] ],
		// type : 'bar',
		// },
		// bar : {
		// width : {
		// ratio : 0.5
		// }
		// }
		// });
		// break;
		// case "pie":
		// chart = c3.generate({
		// bindto : '#chart',
		// data : {
		// columns : [ [ 'data1', 30, 200, 100, 400, 150, 250 ],
		// [ 'data2', 50, 20, 10, 40, 15, 25 ] ],
		// type : 'pie'
		// }
		// });
		// break;
		// }
		return true;
	}
}

/*
 * 
 */
function enableChartOptions(){}


/* =========== END of fardeen's code ======================================================================= */
</script>
</body>
</html>