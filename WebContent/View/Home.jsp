<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>HOBS Analytics and Visualization tool</title>
<link rel="stylesheet" href="../resources/css/jquery.steps.css" />
<link rel="stylesheet" href="../resources/css/main.css" />
<link rel="stylesheet" href="../resources/css/normalize.css" />
<link rel="stylesheet" href="../resources/css/bootstrapNEW.min.css" />
<link rel="stylesheet" href="../resources/css/w3.css" />
<link rel="stylesheet" href="../resources/css/jquery-ui.min.css" />
<link rel="stylesheet" href="../resources/css/loader.css" />
<link rel="stylesheet" href="../resources/css/bootstrap-multiselect.css" />
<link rel="stylesheet" href="../resources/css/bootstrap-multiselect.css" />
<link rel="stylesheet" href="../resources/css/bootstrap-3.1.1.min.css"
	type="text/css" />

<!-- Adding C3.js css file -->
<link rel="stylesheet" href="../resources/css/c3.min.css" />
<!-- Adding C3.js and D3.js  file -->
<script type="text/javascript" src="../resources/js/d3.min.js"></script>
<script type="text/javascript" src="../resources/js/c3.min.js"></script>
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


<script type="text/javascript" src="../resources/js/jquery-3.x.js"></script>
<script type="text/javascript" src="../resources/js/jquery.steps.min.js"></script>
<script type="text/javascript" src="../resources/js/jquery.cookie.js"></script>
<script type="text/javascript"
	src="../resources/js/jquery.validate.min.js"></script>

<script type="text/javascript"
	src="../resources/js/modernizr-2.6.2.min.js"></script>
<script type="text/javascript" src="../resources/js/jquery-ui.min.js"></script>
<script type="text/javascript"
	src="../resources/js/bootstrap-multiselect.js"></script>
<script type="text/javascript"
	src="../resources/js/bootstrap-2.3.2.min.js"></script>

<script src="../resources/js/dropzone.js"></script>
<link rel="stylesheet" href="../resources/css/dropzone.css">
<link rel="stylesheet" href="../resources/css/customStyle1.css">
<script src="../resources/js/customeScript1.js"></script>

<!-- For Color palate -->
<link href="../resources/css/colorpickerdemo.css" rel="stylesheet" />
<link href="../resources/css/evol-colorpicker.min.css" rel="stylesheet" />
<script src="../resources/js/evol-colorpicker.min.js"
	type="text/javascript"></script>

<!-- Dependencies for step 1 and 2 -->
<script src="../resources/js/query-builder.standalone.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />

</head>
<body>
	<div class="">
		<form id="the-form">
			<input type="hidden" name="hf_current_step" id="hf_current_step" />
			<input type="hidden" name="hf_datasource" id="hf_datasource" /> <input
				type="hidden" name="hf_are_files_uploaded"
				id="hf_are_files_uploaded" value="no" /> <input type="hidden"
				name="hf_canGoFromStep0" id="hf_canGoFromStep0" value="0" />

			<div class="header bg-dark-custom">
				<h3 class="w3-text-white">HOBS Data Analytics and Visualization</h3>
			</div>

			<div class="main-content">

				<!-- The Wizard ================================== =====================================================  -->
				<div id="wizard" class="w3-container">

					<!-- STEP - 1 : Select Dataset =====================================================  -->
					<h1>Select Dataset</h1>
					<section style="width: 100%;">
						<div class="">

							<div class="radio ">
								<label><input type="radio" class="radio_data_source"
									name="radio_data_source" value="file">Upload File</label>
							</div>
							<div class="radio ">
								<label><input type="radio" class="radio_data_source"
									name="radio_data_source" value="database">Select
									Database Configuration</label>
							</div>
							<div class="radio ">
								<label><input type="radio" class="radio_data_source"
									name="radio_data_source" value="logical_dataset">Select
									Logical Dataset</label>
							</div>
							<!-- <input type="radio" class="rr" name="radio_daata_source" value="file" >Upload File -->
							<div style="width: 100%; height: 1px;" class="bg-primary"></div>
							<br>

							<!-- Data Source Option FILE UPLOAD =====================================================  -->
							<div id="data_source_file" class="w3-hide">
								<div class="w3-row-padding">
									<div class="w3-col m5 l4">
										<div id="myFileUpload" class="dropzone-fu"
											style="width: 100%; border: 3px solid grey; cursor: pointer;">
											<div class="dz-message">
												Click here to upload files <br> OR Simply drag and drop
												your files here.
											</div>
											<!-- Upload file from here.. -->
										</div>
										<!-- 	<form id="my-awesome-dropzone" action="../upload-file"
										class="dropzone" method="POST"></form> -->
									</div>
									<div class="w3-col m7  l8">
										calm down.... table will be displayed here...
										<div class="table-responsive w3-border"
											style="background-color: white">
											<table id="file_list_table" class="table table-bordered">
												<tr class="table-primary">
													<th>File name</th>
													<th>Row Delimiter</th>
													<th>Column Delimiter</th>
													<th>Header Row Index</th>
													<th>&nbsp;</th>
												</tr>

											</table>
										</div>

									</div>
								</div>
								<!-- <br> <input type="button"
									class="btn btn-lg btn-primary active w3-right" value="Next"
									onclick="check_datasource_file" /> -->
							</div>
							<!-- END OF Data Source Option FILE UPLOAD =====================================================  -->

							<!-- Data Source Option Database =====================================================  -->
							<div id="data_source_database" class="w3-hide">

								<label for="database_configuration">Select a Database
									Configuration: </label> <select name="database_configuration"
									id="database_configuration">

								</select> <br> <label for="table_list">Select Tables: </label> <select
									name="table_list" id="table_list" multiple="multiple">

								</select>

							</div>
							<!-- END OF Data Source Option Database =====================================================  -->

							<!-- Data Source Option Logical Dataset =====================================================  -->
							<div id="data_source_logical_dataset" class="w3-hide">
								Logical</div>
							<!-- END OF Data Source Option Logical Dataset =====================================================  -->

						</div>
					</section>
					<!-- END OF STEP - 1 : Select Datatset =====================================================  -->

					<!-- STEP - 2 : View Data =====================================================  -->
					<h1>View Data</h1>
					<section>
						<div class="w3-row-padding">
							<div class="w3-col m7  l8" id="tablesData1">
								<div class="table-responsive w3-border"
									style="background-color: white"></div>

							</div>


							<div class="w3-col m5 l4">
								<div class="table table-responsive w3-border"
									id="file_table_name"
									style="background-color: white; height: 250px; overflow: scroll; border-radius: 7px;">


								</div>
								<div class="table table-responsive">

									<!--============================================================== Start Of Model=================================================================================== -->


									<div style="float: right">
										<input type="button" value="Open Query Builder"
											onclick="document.getElementById('id01').style.display='block'"
											class="w3-button w3-round-large">
									</div>
									<br>
									<div id="id01" class="w3-modal">
										<div class="w3-modal-content w3-animate-opacity w3-card-4">
											<header class="w3-container w3-teal">
												<span
													onclick="document.getElementById('id01').style.display='none'"
													class="w3-button w3-display-topright">&times;</span>
												<h2>Query Builder</h2>
											</header>
											<div class="w3-container">


												<!-- =====================Start Query Builder================================================= -->


												<div id="builder" style="margin-top: 50px;"></div>

												<div id="result">

													<div id="rbtn" style="float: right;">

														<input type="button" value="Genrate Sql Query"
															class="btn btn-primary btn-get-sql"
															onclick="getSqlQuery()"> <input type="button"
															class="btn btn-primary btn-get-rules" Value="getrules"
															onclick="getFileQuery()" />

													</div>

												</div>
												<!--============================================================== End Query Builder=================================================================================== -->
											</div>
										</div>
									</div>
									<!--============================================================== End Of Model=================================================================================== -->
									<br>
									<fieldset>
										<legend>
											<b>Query</b>
										</legend>
										<input class="w3-input w3-border w3-round-large"
											id="queryshow" type="text" readonly="readonly"> <input
											class="w3-input w3-border w3-round-large" id="queryshow1"
											type="text" readonly="readonly">

									</fieldset>

									<br>
									<div style="float: right">
										<input type="button" value="Create Relation"
											onclick="createRelation()" class="w3-button w3-round-large">
									</div>
								</div>

							</div>
						</div>

					</section>
					<!-- END OF STEP - 2 : View Data =====================================================  -->
					<!-- STEP - 3 : Visualize =====================================================  -->
					<h1>Visualize</h1>
					<section id="chart-container">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<div class="row" id="chart-type-row">
										<div class="col-md-5 col-xs-5">
											<div id="chart-type-container">
												<h3 id="chart-type-title">Select a Chart Type</h3>
												<div id="chart-types">
													<table>
														<tr>
															<td><span><span class="chart-type-block">
																		<img src="../resources/img/charts/line-chart.png">
																</span> <span class="chart-type-title">Line</span></span></td>
															<td><span><span class="chart-type-block"><img
																		src="../resources/img/charts/pie-chart.png"></span> <span
																	class="chart-type-title">Pie</span></span></td>
															<td><span><span class="chart-type-block"><img
																		src="../resources/img/charts/bar-chart.png"></span> <span
																	class="chart-type-title">Bar</span></span></td>
															<td><span><span class="chart-type-block"><img
																		src="../resources/img/charts/area-chart.png"></span> <span
																	class="chart-type-title">Area</span></span></td>
															<td><span><span class="chart-type-block"><img
																		src="../resources/img/charts/scatter-chart.png"></span>
																	<span class="chart-type-title">Scatter</span></span></td>
														</tr>

													</table>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="row" id="chart-options-row">
									<div class="col-md-12 col-xs-12">
										<h3>
											<span> <i id="back-to-chart-type"
												class="fa fa-arrow-left"></i>

											</span>Chart Options
										</h3>
										<div class="row">
											<div class="col-md-5 col-xs-5">
												<div class="row chart-options">
													<!-- Display selected chart options here -->
													<div class="col-md-12 col-xs-12" id="chart-options">
														<p style="padding: 5px"></p>
														<div class="row chart-options-menu">
															<span class="btn">Data</span> <span class="btn">Display</span>
															<span class="btn">Axes</span> <span class="btn">Label</span>
														</div>
														<br>
														<div id="chart-single-option">

															<div id="label-option"
																style="width: 250px; height: 300px; overflow: auto">
																<table>
																	<tr>
																		<th colspan='2'>Axis Labels</th>
																	<tr>
																	<tr>
																		<td>X-axis</td>
																		<td><input type="text" id="x-label">
																	<tr>
																	<tr>
																		<td>Y-axis</td>
																		<td><input type="text" id="y-label">
																	<tr>
																	<tr>
																		<th colspan='2'>tick options</th>
																	<tr>
																	<tr>
																		<td>count</td>
																		<td><input type="text" id="tick-count"
																			title="The number of x axis ticks to show"></td>
																	<tr>
																	<tr>
																		<td>rotate</td>
																		<td><input type="text" id="tick-rotate"
																			title="rotate x axis tick names"></td>
																	<tr>
																	<tr>
																		<td>max</td>
																		<td><input type="text" id="tick-max"
																			title="The number of tick texts will be adjusted to less than this value">
																		</td>
																	<tr>
																	<tr>
																		<td colspan=2><Button type="button"
																				id="tick-submit" class="btn btn-primary">Done
																				ticking</Button></td>
																	<tr>
																</table>

															</div>

															<div id="axes-option"
																style="width: 250px; height: 300px; overflow: auto">
																<table>
																	<tr>
																		<th>Ranges</th>
																	<tr>
																	<tr>
																		<td rowspan='2'>X-range</td>
																		<td><input type="text" id="xmin"
																			placeholder="min">
																	<tr>
																		<td><input type="text" id="xmax"
																			placeholder="max">
																	<tr>
																		<td rowspan='2'>Y-range</td>
																		<td><input type="text" id="ymin"
																			placeholder="min">
																	<tr>
																		<td><input type="text" id="ymax"
																			placeholder="max">
																	<tr>
																	<tr>
																		<td colspan=2><Button type="button"
																				id="axes-submit" class="btn btn-primary">Done
																				Ranging</Button></td>
																	<tr>
																	<tr>
																		<th>Font size</th>
																	<tr>
																	<tr>
																		<td>X axis</td>
																		<td><input type="text" list="numbers"
																			id="axes-x-font-size"> <datalist id="numbers">
																				<option>10</option>
																				<option>12</option>
																				<option>14</option>
																				<option>16</option>
																				<option>20</option>
																				<option>22</option>
																				<option>25</option>
																			</datalist></td>
																	<tr>
																		<td>Y axis</td>
																		<td><input type="text" list="numbers"
																			id="axes-y-font-size"> <datalist id="numbers">
																				<option>10</option>
																				<option>12</option>
																				<option>14</option>
																				<option>16</option>
																				<option>20</option>
																				<option>22</option>
																				<option>25</option>
																			</datalist></td>
																	<tr>
																	<tr>
																		<th>Font Family</th>

																		<td><input type="text" list="fonts"
																			id="axes-font-family"> <datalist id="fonts">
																				<option>Arial</option>
																				<option>Comic Sans MS</option>
																				<option>Trebuchet MS</option>
																				<option>Verdana</option>
																				<option>Averia Sans Libre</option>
																				<option>Righteous</option>
																				<option>Sancreek</option>
																				<option>Alegreya SC</option>
																				<option>Merienda One</option>
																				<option>Aldrich</option>
																				<option>Nothing You Could Do</option>
																			</datalist></td>
																	<tr>

																		<th>Rotated</th>
																		<td><input id="axes-rotate" class="inline"
																			type="checkbox" name="rotated"></td>
																	<tr>
																	<tr>
																		<th>Grid</th>
																	<tr>
																	<tr>
																		<td>X <input class="axes-grid" id="axes-grid-x"
																			type="checkbox" name=""></td>
																		<td>Y <input class="axes-grid" id="axes-grid-y"
																			type="checkbox" name=""></td>
																	<tr>
																</table>

															</div>

															<div id="display-option"
																style="width: 250px; height: 300px; overflow: auto">

																<table>

																	<tr>
																		<th>Color Properties</th>
																	<tr>
																	<tr>
																		<td>
																			<div id="color-properties"></div>
																		</td>
																	<tr>
																	<tr>
																		<th>Types</th>
																	<tr>
																	<tr>
																		<td>
																			<div id="type-properties"></div>
																		</td>
																	</tr>
																	<tr>
																		<td><Button type="button" id="display-submit"
																				class="btn btn-primary">Done</Button></td>
																	</tr>
																	<!-- <tr>
																		<th>Resize</th>
																	<tr>
																	<tr>
																		<td><span style="display: inline-block">Height<input
																				type="text" id="graph-height"></span></td>
																	<tr>
																	<tr>
																		<td><span style="display: inline-block">Width<input
																				type="text" id="graph-width"></span></td>
																	<tr> -->
																	<tr>
																		<td><span style="display: inline-block">Background
																				color<input type="text" id="graph-bgcolor">
																		</span></td>

																	</tr>

																	<tr>
																		<th>Data Labels &nbsp; <input id="display-labels"
																			class="inline" type="checkbox" name="labels"></th>
																	</tr>
																	<tr>
																		<th colspan="2">Legend Position</th>
																	</tr>
																	<tr>
																		<td>
																			<div id="myradio">
																				<label><input type="radio"
																					name="legendPosition" value="bottom">Bottom</label>
																			</div>
																		</td>
																		<td>
																			<div>
																				<label><input type="radio"
																					name="legendPosition" value="right" checked>Right</label>
																			</div>
																		</td>
																	</tr>

																</table>
															</div>

															<div id="data-option"
																style="width: 250px; height: 300px; overflow: auto">
																Select fields
																<div id="data-fields"></div>
																<div>
																	<Button type="button" id="data-submit"
																		class="btn btn-primary">Done</Button>
																</div>
															</div>


														</div>

													</div>
												</div>
												<div id="submit-chart-options" style="text-align: center;">
													<button class="btn btn-primary" style="width: 80%;">Done</button>
												</div>
											</div>
											<div class="col-md-1 col-xs-1"></div>
											<div class="col-md-6 col-xs-6 chart-zone">
												<div id="chart"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
					<!-- END OFSTEP - 3 : Visualize =====================================================  -->
					<!-- STEP - 4 : Publish =====================================================  -->
					<h1>Publish</h1>
					<section>
						<div class="form-group">
							<div class="row">
								<div class="col-sm-4">
									<label for="chart_name"><b>Chart Name:</b></label> <input
										type="text" name="chart_name" id="chart_name"
										class="form-control" />
								</div>
							</div>
							<br />

							<div class="row">
								<div class="col-sm-4">
									<label for="save_to_workspace"><b>Save To
											Workspace:</b></label> <select name="workspace" id="workspace"
										class="form-control">
										<option value="Select">Select Workspace..</option>
										<option value="current_workspace">Current Workspace</option>
										<option value="create_new_workspace">Create New
											Workspace</option>
									</select>
								</div>
							</div>
							<br>

							<div class="row" id="newworkspace" class="newworkspace"
								style="display: none;">
								<div class="col-sm-4">
									<label for="workspace_name"><b>Workspace Name:</b></label> <input
										type="text" name="workspace_name" id="workspace_name"
										class="form-control" />
								</div>
							</div>
							<div id="demo-print"></div>
							<br> <br> <br> <br> <br> <br></br> <br>
							<br>

							<div class="container-login100-form-btn" align="right">
								<button class="btn btn-success" id="publish">Publish</button>

							</div>
						</div>
					</section>
					<!-- END OF STEP - 4 : Publish =====================================================  -->

				</div>
				<!-- END OF The Wizard ================================== =====================================================  -->

			</div>

		</form>
	</div>
	<br>
	<br>

	<!-- Loader  ============================================== -->
	<div id="the_loader">
		&nbsp;&nbsp;
		<div class="lds-spinner">
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<br>Loading
		</div>
	</div>
	<!-- END OF Loader  ============================================== -->



</body>
</html>