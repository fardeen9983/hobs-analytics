/*
 * Global Variables
 */
var data;
var SelectedColumn = [];
var storeData = [];
var fileTableNames = [];
var querytobefired;
var myrules;
var filemanu;
var form = $("#the-form");

// Chart Variables
var onlyData = true;
var config = setConfig();
var jsonData = [ {
	'data1' : 30,
	'data2' : 50
}, {
	'data1' : 200,
	'data2' : 20
}, {
	'data1' : 100,
	'data2' : 10
}, {
	'data1' : 400,
	'data2' : 40
}, {
	'data1' : 150,
	'data2' : 15
}, {
	'data1' : 250,
	'data2' : 25
} ];

// END OF GLOBAL VARIABLES

/*
 * File Upload using DropZone ============================================
 */

form.validate({
	errorPlacement : function errorPlacement(error, element) {
		element.before(error);
	}
});
Dropzone.autoDiscover = false;
var myDropzone;
$(document).ready(function() {
	$(function() {
		$("#myFileUpload").dropzone({
			url : "../FileUploadHandler",
			addRemoveLinks : true,
			autoQueue : false,
			enqueueForUpload : false,
			dictInvalidFileType : "Invalid file type"

		});

	});
});

Dropzone.options.myFileUpload = {
	acceptedFiles : 'text/csv,text/plain,text/tab-separated-values,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',

	init : function() {
				this
						.on(
								"addedfile",
								function(file) {
									var isFileAllowed = 1;
									// alert("File Added.");

									if (this.files.length - 1 == 0) {
										// alert("first file added");
										this.processFile(file);
										return true;
									}
									// return false;
									for (j = 0, lj = this.files.length; j < lj - 1; j++) {
										// alert(this.files[j].name + "\n == \n"
										// + file.name);
										if (this.files[j].name == file.name) {

											alert(file.name
													+ " found to be duplicate and is prevented from being uploaded to the server. \n\n If you still want to upload this file then you can either... \n\n 1. Delete the existing file from server. \n OR \n 2. Rename this file and try to upload it. ");
											isFileAllowed = 0;
											this.removeFile(file);
										}
									}
									if (isFileAllowed == 1) {
										this.processFile(file);
										// return true;
									}
									// alert("will return false now");
									return false;

								}), // END OF on addedFile event
				this
						.on(
								"success",
								function(file, response) {
									// alert(file.name);
									// alert(response);
									res = response.split(',');
									// alert(res[0] + "\n" + res[1] + "\n" +
									// res[2]);
									if (res != null && res[0] == "success") {
										$("#file_list_table")
												.append(
														"<tr id=\"row_"
																+ res[1]
																+ "\"> <td> "
																+ file.name
																+ " </td>  <td> <input id=\" rd_"
																+ file.name
																+ " \" class=\"row-delimiter\"   required/> <span class=\"err-message err-message-rd\"></span> </td> <td>  <input id=\" cd_"
																+ file.name
																+ " \" class=\"column-delimiter\"   required/> <span class=\"err-message err-message-cd\"></span>  </td> <td> <input type=\"number\" min=\"0\" id=\" hri_"
																+ file.name
																+ " \" class=\"header-row-index\"   required/> <span class=\"err-message err-message-hri\"></span>  </td> <td> <input type=\"button\" class=\"btn btn-danger\" value=\"Delete\" onclick=\"deleteFile('"
																+ res[2]
																+ "','"
																+ res[1]
																+ "') \"  /> </td>  </tr>");
									} else {
										alert("Failed to upload '" + file.name
												+ "''");
										this.removeFile(file);
									}
								}),
				/* end of onSuccess ============================= */
				this.on("complete", function(file) {
					// this.removeFile(file);
					// file.previewElement.innerHTML = "";
					file.previewElement.parentNode
							.removeChild(file.previewElement);
					$(".dz-message").addClass("w3-show");
				});
	}
}

if (document.getElementById('myFileUpload')) {
	// alert("found");
	// myDropzone = new Dropzone("div#myFileUpload", {url : "../upload-file"});
	// other code here
} else {
	// alert("nnn");
}

Dropzone.options.myDropzone = {

	success : function(file, done) {
		/*
		 * total_photos_counter++; $("#counter").text("# " +
		 * total_photos_counter);
		 */
		// alert(file.name + " uploaded successfully");
	}
};
/*
 * END of Dropzone file upload
 */

/*
 * Wizard configuration =================================================
 */
$(function() {

	var wizard = $("#wizard").steps({
		headerTag : "h1",
		bodyTag : "section",
		transitionEffect : "slideLeft",
		onStepChanging : function(event, currentIndex, newIndex) {
			if (canChangeStep())
				return true;
			else
				return false;
			return true;
		},
		onStepChanged : function(event, currentIndex, priorIndex) {
			onStepBeginnig();
		},
		onFinished : function() {
			window.location.href = "./ConfigureDashboard.jsp";
		}
	});
	/*
	 * END OF wizard configuration
	 * =================================================
	 */

	/* goto specific step on start ======== */
	/*
	 * gotoStep = 2; for(i=0; i <gotoStep; i++){ wizard.steps("next"); }
	 */
	/* END OF goto specific step on start === */

});

/*
 * This function decides whether user can change the step or not
 * ====================
 */
function canChangeStep() {
	// return true;
	var canChange = false;
	// alert("step changing");
	currentStep = $("#wizard").steps("getCurrentIndex");

	if (currentStep == 0) { /*
							 * ======================== step-1 validation
							 * ==================
							 */
		// return canChangeStep0();
		return true;
	} /* end of step-1 validation ========================================= */
	else if (currentStep == 1) { /*
									 * ======================== step-2
									 * validation ==================
									 */
		/*
		 * call your function for custom validation. refer to step-1 validation
		 * for example. the function should return either true or false, here
		 * true indicates step changing is allowed and false indicates step
		 * changing is is not allowed
		 */
		return true;
	} /* end of step-2 validation ========================================= */
	else if (currentStep == 2) { /*
									 * ======================== step-3
									 * validation ==================
									 */
		/*
		 * call your function for custom validation. refer to step-1 validation
		 * for example. the function should return either true or false, here
		 * true indicates step changing is allowed and false indicates step
		 * changing is is not allowed
		 */
		return true;
	} /* end of step-3 validation ========================================= */
	else { /* ======================== step-4 validation ================== */
		/*
		 * call your function for custom validation. refer to step-1 validation
		 * for example. the function should return either true or false, here
		 * true indicates step changing is allowed and false indicates step
		 * changing is is not allowed
		 */
		return true;
	} /* end of step-4 validation ========================================= */

	// return canChange;

} /* END OF canChangeStep() ======================= ============ */

/*
 * onStepBeginnig() is called starting of each step. Call your custom function
 * here to do things on start of the step ===================
 */
function onStepBeginnig() {
	currentStep = $("#wizard").steps("getCurrentIndex");
	if (currentStep == 0) { /*
							 * ======================== step-1 init
							 * ==================
							 */

	} /* end of step-1 init ========================================= */
	else if (currentStep == 1) { /*
									 * ======================== step-2 init
									 * ==================
									 */

		// alert(data);

	} /* end of step-2 init ========================================= */
	else if (currentStep == 2) { /*
									 * ======================== step-3 init
									 * ==================
									 */

	} /* end of step-3 init ========================================= */
	else { /* ======================== step-4 init ================== */

	} /* end of step-4 init ========================================= */

}
/*
 * END OF can onStepBeginning() ==========
 * ========================================
 */

// End of Function
var fromWhereTheNextIsEmmited = "self";
/* can Change Step 0 ============================================ */
function canChangeStep0() {
	var datasource = $("#hf_datasource").val();
	if (datasource == null || datasource == "") {
		alert("Please select a datasource to proceed furthure");
		return false;
	} else {
		if (datasource == "file") {
			if (fromWhereTheNextIsEmmited == "self") {
				return validateDatasourceFile();
			} else {
				if (fromWhereTheNextIsEmmited == "response") {
					fromWhereTheNextIsEmmited = "self";
					return true;
				}
			}
		} else if (datasource == "database") {
			return validateDatasourceDatabase();
		} else if (datasource == "logical_dataset") {
			return true;
		} else {
			return false;
		}
	}
}
/* END OF can Change Step 0 ======================================== */

/*
 * STEP 1
 * ================================================================================================================================
 */

/* Validate Datasource File ============================================= */
function validateDatasourceFile() {
	form = $("#the-form");
	form.validate().settings.ignore = ":disabled,:hidden";

	if (form.valid()) {
		var f = $('#file_list_table td:first-child');
		var rd = $(".row-delimiter");
		var err_rd = $(".err-message-rd");
		var cd = $(".column-delimiter");
		var err_cd = $(".err-message-cd");
		var hri = $(".header-row-index");
		var err_hri = $(".err-message-hri");

		// alert(f);
		if (f == null || f.length <= 0) {
			alert("Please upload at least one file to proceed furthure");
			return false;
		}
		// alert(f.length);
		files_uploaded = "";
		row_delimiters = "";
		column_delimiters = "";
		header_row_indices = "";
		flag = 1;

		for (i = 0; i < f.length; i++) {
			files_uploaded += f[i].innerHTML

			if (rd[i].value == null || rd[i].value == " "
					|| rd[i].value.trim().length <= 0) {
				flag = 0;
				// rd[i].classList.add("error");
				err_rd[i].innerHTML = "Value Required";
				// break;
			} else {
				rd[i].classList.remove("error");
				err_rd[i].innerHTML = "";
				row_delimiters += rd[i].value;
			}

			/* for column delimiter ==================== */
			if (cd[i].value == null || cd[i].value == " "
					|| cd[i].value.trim().length <= 0) {
				flag = 0;
				// rd[i].classList.add("error");
				err_cd[i].innerHTML = "Value Required";
				// break;
			} else {
				cd[i].classList.remove("error");
				err_cd[i].innerHTML = "";
				column_delimiters += cd[i].value;
			}
			/* for column separator ==================== */

			/* for column delimiter ==================== */
			if (hri[i].value == null || hri[i].value == " "
					|| hri[i].value.trim().length <= 0) {
				flag = 0;
				// rd[i].classList.add("error");
				err_hri[i].innerHTML = "Value Required";
				// break;
			} else {
				hri[i].classList.remove("error");
				err_hri[i].innerHTML = "";
				header_row_indices += hri[i].value;
			}
			/* for column separator ==================== */

			if (i != f.length - 1) {
				files_uploaded += "~";
				row_delimiters += "~";
				column_delimiters += "~";
				header_row_indices += "~";
			}
		}
		// alert(files_uploaded + "\n\n" + column_delimiters + "\n\n" +
		// row_delimiters + "\n\n" + header_row_indices);
		if (flag == 1) {
			// alert(files_uploaded + "\n\n" + column_delimiters + "\n\n" +
			// row_delimiters + "\n\n" + header_row_indices);
			var canGo = 0;
			$
					.ajax({
						url : 'http://localhost:8080/HOBSAnalyticVisu/rest/file/setFile/'
								+ files_uploaded
								+ "/"
								+ column_delimiters
								+ "/"
								+ row_delimiters
								+ "/"
								+ header_row_indices,
						type : 'POST',
						dataType : 'JSON',

						success : function(response, status) {
							// alert("setFile \n" + status + "\n" + response);
							$("#hf_canGoFromStep0").val("waiting_for_response");

							if (status == "success") {
								data = Array();
								data[0] = "file";
								data[1] = response;
								$("#hf_canGoFromStep0").val("true");
								fromWhereTheNextIsEmmited = "response";
								$("#wizard").steps("next");
								canGo = 1;
								/* goto myLabel; */
								// alert($("#hf_canGoFromStep0").val() + " will
								// be returned");
								/*
								 * if ($("#hf_canGoFromStep0").val() == "1" ||
								 * canGo == 1) return true; else return false;
								 */
							} else {
								alert("error \n status != sucess")
								$("#hf_canGoFromStep0").val("false");
								/*
								 * $("#hf_canGoFromStep0").val("0"); canGo = 0;
								 */
							}
						},
						error : function(response, status) {
							alert("setFile \n" + "response error");
							$("#hf_canGoFromStep0").val("false");
							// $("#hf_canGoFromStep0").val("0");
							// alert("response error \n" + status + "\n" +
							// response);
							// return false;
						}
					});
			// return loopTillRespond();
			// alert("after function call");
			/*
			 * if ($("#hf_canGoFromStep0").val() == "true") { return true; }
			 * else { return false; }
			 */

		} else
			return false;
	} else {
		return false;
	}

}
/* END OF Validate Datasource File ============================================= */

function loopTillRespond() {
	if ($("#hf_canGoFromStep0").val() == "no") {
		loopTillRespond();
	}
	if ($("#hf_canGoFromStep0").val() == "true") {
		return true;
	} else {
		return false;
	}
}

function validateDatasourceDatabase() {
	form = $("#the-form");
	var selectedTables = $("#table_list").val();
	// alert("="+selectedTables +"=\n"+ selectedTables.length);
	if (selectedTables == null
			|| selectedTables.length <= 0
			|| (selectedTables.length == 1 && selectedTables[0] == "multiselect-all")) {
		// document.getElementById("the_loader").classList.remove("w3-hide");
		alert("Please select a table to proceed furthure");
		return false;
	} else {
		data = Array();
		data[0] = "database";
		data[1] = $('#database_configuration').val();
		data[2] = selectedTables;
		return true;
	}

}

/* file delete function =========================================== */
function deleteFile(name, name2) {
	// alert(name);
	if (confirm("Are you sure you want to delete '" + name + "'?")) {
		$
				.ajax({
					url : 'http://localhost:8080/HOBSAnalyticVisu/rest/tables/deleteFile/'
							+ name,
					type : 'POST',
					dataType : 'JSON',

					success : function(response, status) {
						// alert(status + "\n" + response);
						if (status == "success") {
							// alert("deleted from server... removing from table
							// now");
							$("#file_list_table #row_" + name2).remove();
							// alert("removed???");
						} else {
							alert("error");
						}
					},
					error : function(response, status) {
						alert("response error");
						// alert("response error \n" + status + "\n" +
						// response);
					}

				});
	}
}
/* file delete function =========================================== */

/* start of document.ready =========================================== */
$(document)
		.ready(
				function() {
					function showTheLoader() {
						document.getElementById("the_loader").classList
								.remove("w3-hide");
					}

					function hideTheLoader() {
						document.getElementById("the_loader").classList
								.add("w3-hide");
					}

					document.getElementById("the_loader").classList
							.add("w3-hide");

					$('#table_list').multiselect({
						includeSelectAllOption : true,
						enableCaseInsensitiveFiltering : true,
						maxHeight : 200
					});
					$("#table_list").hide();

					/*
					 * radio button click event
					 * ====================================================
					 */
					$(".radio_data_source")
							.click(
									function() {
										showTheLoader();
										document.getElementById("the_loader").classList
												.remove("w3-hide");

										// alert("option radio clicked");
										var selectedOption = $(this).attr(
												"value");
										$("#hf_datasource").val(selectedOption);

										// alert(selectedOption);

										if (selectedOption == "file") {
											$("#data_source_file").removeClass(
													"w3-show");
											$("#data_source_database")
													.removeClass("w3-show");
											$("#data_source_logical_dataset")
													.removeClass("w3-show");

											$("#data_source_file").addClass(
													"w3-show");
										} else if (selectedOption == "database") {
											$("#data_source_file").removeClass(
													"w3-show");
											$("#data_source_database")
													.removeClass("w3-show");
											$("#data_source_logical_dataset")
													.removeClass("w3-show");

											$("#data_source_database")
													.addClass("w3-show");

											/*
											 * get the list of database
											 * configurations using REST
											 * =================================
											 */
											$
													.ajax({
														url : 'http://localhost:8080/HOBSAnalyticVisu/rest/tables/getDbConfig',
														type : 'get',
														dataType : 'JSON',

														success : function(
																response,
																status) {
															let dropdown = $('#database_configuration');
															dropdown.empty();
															// alert($('#table_list').val());
															let dropdown2 = $('#table_list');
															dropdown2.empty();
															// $('#table_list').empty().multiSelect('refresh');
															$(
																	"#table_list option:selected")
																	.prop(
																			"selected",
																			false);
															// alert("deselect
															// \n" +
															// $('#table_list').val());
															$(
																	"#table_list option")
																	.remove();
															// alert("remove()
															// \n" +
															// $('#table_list').val());

															dropdown
																	.append('<option selected="true" disabled>Select DB Config</option>');
															dropdown
																	.prop(
																			'selectedIndex',
																			0);
															dropdown2
																	.prop(
																			'selectedIndex',
																			0);

															for (i in response) {
																dropdown
																		.append($(
																				'<option></option>')
																				.attr(
																						'value',
																						response[i].type)
																				.text(
																						response[i].configname));

															}
															$('#table_list')
																	.empty();
															$('#table_list')
																	.multiselect(
																			'rebuild');
														}
													});
											/*
											 * END OF loading the list of
											 * database configurations using
											 * REST
											 * =================================
											 */

										} else {
											$("#data_source_file").removeClass(
													"w3-show");
											$("#data_source_database")
													.removeClass("w3-show");
											$("#data_source_logical_dataset")
													.removeClass("w3-show");

											$("#data_source_logical_dataset")
													.addClass("w3-show");
										}

										hideTheLoader();
										document.getElementById("the_loader").classList
												.add("w3-hide");
									}); /*
										 * END OF radio button click event
										 * ====================================================
										 */

					/*
					 * database configuration change event
					 * ==================================================
					 */
					$('#database_configuration')
							.change(
									function() {
										showTheLoader();
										document.getElementById("the_loader").classList
												.remove("w3-hide");

										var ival = $(
												'#database_configuration :selected')
												.val();
										if (ival == 1) {

											$
													.ajax({
														url : 'http://localhost:8080/HOBSAnalyticVisu/rest/tables/getOTableList',
														type : 'get',
														dataType : 'JSON',

														success : function(
																response,
																status) {

															let dropdown = $('#table_list');

															dropdown.empty();

															for (i in response) {
																$('#table_list')
																		.append(
																				"<option value='"
																						+ response[i]
																						+ "'>"
																						+ response[i]
																						+ "</option>");

																$('#table_list')
																		.multiselect(
																				'rebuild');

															}

															$('#table_list')
																	.multiselect(
																			{
																				includeSelectAllOption : true,
																				enableCaseInsensitiveFiltering : true,
																				maxHeight : 200
																			});
															// $('#db_table').multiselect('refresh');
														}
													});
										}
										if (ival == 3) {

											$
													.ajax({
														url : 'http://localhost:8080/HOBSAnalyticVisu/rest/tables/getMTableList',
														type : 'get',
														dataType : 'JSON',

														success : function(
																response,
																status) {

															let dropdown = $('#table_list');

															dropdown.empty();
															for (i in response) {

																$('#table_list')
																		.append(
																				"<option value='"
																						+ response[i]
																						+ "'>"
																						+ response[i]
																						+ "</option>");
																$('#table_list')
																		.multiselect(
																				'refresh');

															}
														}
													});

										}

										$('#table_list').multiselect({

											includeSelectAllOption : true

										});
										hideTheLoader();
										document.getElementById("the_loader").classList
												.add("w3-hide");
									});
					/*
					 * END OF database configuration change event
					 * =============================================
					 */

					$("#table_list").change(function() {

						selectedVal = $("#table_list").val();
						// alert(selectedVal.length);

						if (selectedVal == null || selectedVal.length <= 0) {
							// alert("Please select at least one table");
						}
					});

				});
/*
 * END of document.ready ===========================================
 */

/*
 * STEP 2
 * =========================================================================================================================
 */

// Function For Populating Column From Table
function populateTableColumn() {

	let file_table = $('#file_table_name');
	file_table.empty();
	var table_col = "<div id='accordion'>";

	$
			.ajax({
				url : 'http://localhost:8080/AnalyticVisu/rest/tables/getColumnName/'
						+ data[1].toString() + ',' + data[2].toString(),
				type : 'GET',
				dataType : 'JSON',
				async : false,
				success : function(response, status) {
					result = response;
					for (i in response) {

						var colum = [];
						colum = response[i].toString().split('#');
						fileTableNames.push(colum[0]);
						table_col = table_col + "<h3><b>" + colum[0]
								+ "</b></h3><div>";

						for (var k = 1; k < colum.length; k++) {
							table_col = table_col
									+ "<input type='checkbox' onclick='addtoQueryBuilder(this)' value='"
									+ colum[0] + "#" + colum[k] + "'/>"
									+ colum[k] + "</br>"

						}
						table_col = table_col + "</div>";

					}
					table_col = table_col + "</div>";
				}
			});
	file_table.html(table_col);
	$("#accordion").accordion({
		collapsible : true
	});

}

// end of populating table column

// Function Of Populating Column From File
function populateFileColumn() {

	var result;

	$.ajax({
		url : 'http://localhost:8080/AnalyticVisu/rest/file/getFileNames/'
				+ data[1].toString(),
		type : 'POST',
		dataType : 'JSON',
		async : false,
		success : function(response, status) {
			result = response;

		}
	});

	let file_table = $('#file_table_name');
	file_table.empty();
	var file_column = "<div id='accordion'>";

	for (i in result) {
		var fname = result[i].filename.trim();
		fname = fname.trim();
		fileTableNames.push(fname);

		$
				.ajax({
					url : 'http://localhost:8080/AnalyticVisu/rest/file/getFileColumn/'
							+ result[i].filename.trim() + '/123',
					type : 'POST',
					dataType : 'JSON',
					async : false,
					success : function(iresponse, status) {
						file_column = file_column + "<h3><b>" + fname
								+ "</b></h3><div>";

						var columns = iresponse[0].toString().split(",");
						for (var i = 0; i < columns.length; i++) {

							file_column = file_column
									+ "<input type='checkbox' onclick='addtoQueryBuilder(this)' value='"
									+ fname + "#" + columns[i] + "'/>"
									+ columns[i] + "</br>";
						}
						file_column = file_column + "</div>";

					}
				});

	}
	file_column = file_column + "</div>";
	file_table.html(file_column);
	$("#accordion").accordion({
		collapsible : true
	});

}

/*
 * STEP 3
 * =========================================================================================================================
 */

/*
 * START chart configuration =================================================
 */
$(function() {

	$("#data-submit").click(
			function() {

				var favorite = [];
				var unfavorite = [];
				console.log("data-submit");
				// var favorite = [];
				var temp = [];
				$.each($("#data-fields input[name='data-fields']:checked"),
						function() {
							favorite.push($(this).val());
						});
				$.each($("#data-fields input[name='data-fields']:unchecked"),
						function() {
							unfavorite.push($(this).val());
						});

				for (var i = 0; i < config.allcols.length; i++) {
					// console.log(config.allcols[i][0])
					for (var j = 0; j < favorite.length; j++) {
						if (config.allcols[i][0] == favorite[j]) {
							temp.push(config.allcols[i])
							break
						}
					}
				}

				config.cols = temp;

				config.cols.forEach(function(v) {
					if (config.mykeys.indexOf(v[0]) == -1) {
						config.mykeys.push(v[0]);
					}
				});

				config.chart.load({
					columns : temp,
					unload : unfavorite
				});

			});

	// Display form submit

	$("#display-submit").click(function() {
		colorobj = {};
		typesobj = {};
		config.mykeys.forEach(function(e) {
			colorobj[e] = $('#' + e).val();
		});

		// if (config.chart != null) {
		// console.log(colorobj);
		// config.chart.data.colors(colorobj);
		// }

		config.mykeys.forEach(function(e) {
			typesobj[e] = $('#type-' + e).val();
		});
		config.types = typesobj;
		config.mycolor = colorobj;
		if (config.chart != null) {
			getChartConfig(config.type);
		}
	});

	$("#myradio").change(function() {
		var radioValue = $("input[name='legendPosition']:checked").val();
		config.legendPosition = radioValue;
		if (config.chart != null) {
			getChartConfig(config.type);
		}
	});

	$("#graph-height").keyup(function() {
		var hvalue = $("#graph-height").val()
		if (hvalue == "") {
			height: 300
		}
		if (config.chart != null) {
			config.chart.resize({
				height : hvalue
			})
		}
	});
	$("#graph-width").keyup(function() {
		var wvalue = $("#graph-width").val()
		if (wvalue == "") {
			width: 500
		}
		if (config.chart != null) {
			config.chart.resize({
				width : wvalue
			})
		}
	});

	// display->background-color
	$("#graph-bgcolor").colorpicker({
		hideButton : true,
	});
	$("#graph-bgcolor").change(function() {
		$("#chart").css("background-color", $(this).val());
	});

	// axes->font-size
	$("#axes-x-font-size").keyup(function() {
		// console.log($(this).val());
		$("#chart .c3-axis-x text").css("font-size", $(this).val());
		config.fonts.size["x"] = $(this).val();
	});
	$("#axes-y-font-size").keyup(function() {
		// console.log($(this).val());
		$("#chart .c3-axis-y text").css("font-size", $(this).val());
		config.fonts.size["y"] = $(this).val();
	});

	// axes->font-family
	$("#axes-font-family").keyup(function() {
		// console.log($(this).val());
		$(".c3 svg").css("font-family", $(this).val());
		config.fonts.family = $(this).val();
	});

	// Handle Axes form submit
	// Range
	$("#axes-submit").click(function() {
		// var axisobj = {};
		config.axisobj['max'] = {
			x : $('#xmax').val() * 1,
			y : $('#ymax').val() * 1
		};
		config.axisobj['min'] = {
			x : $('#xmin').val(),
			y : $('#ymin').val()
		};
		var xmam, xmin, ymax, ymin;

		console.log(xmax, xmin, ymax, ymin);
		console.log(config.axisobj);
		if (config.chart != null) {
			config.chart.axis.range(config.axisobj);
		}
	});

	// rotate
	$('#axes-rotate').click(function() {
		config.rotated = $(this).prop("checked")
		if (config.chart != null) {
			getChartConfig(config.type);
		}
	});

	// grid
	$('.axes-grid').click(function() {
		config.grid.x.show = $("#axes-grid-x").prop("checked")
		config.grid.y.show = $("#axes-grid-y").prop("checked")

		if (config.chart != null) {
			getChartConfig(config.type);
		}
	});

	// Data Labels
	$('#display-labels').click(function() {
		config.dataLabels = $(this).prop("checked");

		if (config.chart != null) {
			getChartConfig(config.type);
		}
	});

	// Handle label form submit
	$("#x-label").keyup(function() {
		if (config.chart != null) {
			config.labels.x = $(this).val();
			config.chart.axis.labels(config.labels);

		}
	});

	$("#y-label").keyup(function() {
		if (config.chart != null) {
			config.labels.y = $(this).val();
			config.chart.axis.labels(config.labels);
		}
	});

	$("#tick-submit").click(function() {
		config.mytick = {
			count : $("#tick-count").val(),
			// centered: true,

			rotate : $("#tick-rotate").val(),
			culling : {
				max : $("#tick-max").val()
			},
		// height: $("#tick-height").val()
		};
		if (config.chart != null) {
			getChartConfig(config.type);
		}
	});

	// Hide all chart options initially
	$("#chart-single-option").children().hide();

	// Select Chart type
	$("#chart-types td").click(
			function() {

				var block = $(this).find(".chart-type-block");
				// Get selected chart type through title text
				var type = $(this).find(".chart-type-title").text().trim()
						.toLowerCase();

				// Disable all other options
				$(this).siblings().find(".chart-type-block").css(
						"background-color", "#f6fdfe");
				// Select current chart type
				$(this).find(".chart-type-block").css("background-color",
						"#9ae8f1");
				if (config != null) {
					config = setConfig();
					$(".mycheckbox").prop("checked", false);
				}
				// Generate Chart config and display the chart
				if (config.type != type) {
					$("#chart-options-row").show(400);
					$("#chart-type-row").hide(400);
					getChartConfig(type);
				}
			});

	// Select chart option
	$(".chart-options-menu span").click(function() {
		// Unselect other options
		$(this).siblings().css({
			'background-color' : '#f6fdfe',
			'color' : '#27a7d8'
		});
		$(this).css({
			'background-color' : '#27a7d8',
			'color' : 'white'
		});

		config.option = $(this).text().trim().toLowerCase();
		// Show only selected option
		enableChartOptionForm();

	});

	// Hide all chart options before being clicked
	$(".chart-options").children().hide();

	// Switch between chart type and option selections
	// default option
	$("#chart-type-row").show(400);
	$("#chart-options-row").hide();

	// For back button press
	$("#back-to-chart-type").click(function() {
		$("#chart-options-row").hide(400);
		$("#chart-type-row").show(400);

		config.type = "";

		$("#chart-types").find(".chart-type-block").css({
			'background-color' : '#f6fdfe',
			'color' : '#27a7d8'
		});

		$onlydata = true;

		$("input[type='checkbox']").prop("checked", false);
		$("input[type='text']").val('');
		// $("#chart").css("background-color", "#FFFFFF");
		// $('.chart-options-menu span:contains(' + config.option + ')').css({
		// 'background-color' : '#F6FDFE',
		// 'color' : '#27A7D8'
		// });
	});

	// To handle data-field selection
	$("input[name='data-fields']").change(function() {
		// console.log($(this).val());
		config.chart.destroy();
	});

});
/*
 * RESET CONFIG ========================================
 */
function setConfig() {
	var config = {
		type : "",
		label : {},
		keys : [],
		mycolor : {},
		rotated : false,
		legendPosition : "right",
		grid : {
			x : {
				show : false
			},
			y : {
				show : false
			}
		},
		types : {},
		dataLabels : false,
		mytick : {},
		cols : {},
		mykeys : [],
		axisobj : {},
		fonts : {
			size : {},
			family : []
		}

	};
	return config
}

/*
 * This function is used to generate configuration for chart for step 3 only
 * ====================
 */

function getChartConfig(type) {
	if (jsonData != null) {
		config['type'] = type;
		config['json'] = jsonData;
		config['keys'] = Object.keys(jsonData[0]);
		config['chart'] = null;
		config['bindTo'] = "#chart";
		config['labels'] = {
			x : '',
			y : '',
			y2 : ''
		};
		config['size'] = {
			width : 500,
			height : 300
		};

		// Generate columns from data
		var temp = [];
		config.keys.forEach(function(e) {
			temp.push([ e ]);
		});

		for (var i = 0; i < jsonData.length; i++) {
			var data = jsonData[i];
			for (var j = 0; j < temp.length; j++) {
				temp[j].push(data[temp[j][0]]);
			}
		}
		config['allcols'] = temp;

		// Call chart generation
		return generateChart();
	} else
		return false;
}

/*
 * This function is used to generate the chart based on config in step 3
 * ====================
 */

function generateChart() {

	if (config.hasOwnProperty('type')) {
		// Using the config object generate chart
		config.chart = c3.generate({
			bindto : config.bindTo,
			data : {
				// x : 'data1',
				columns : config.cols,
				type : config.type,
				colors : config.mycolor,
				types : config.types,
				labels : config.dataLabels
			},
			legend : {
				position : config.legendPosition
			},
			axis : {
				x : {
					tick : config.mytick,
					label : {
						text : '',
						position : 'outer-center'
					}
				},
				y : {
					label : {
						text : '',
						position : 'outer-middle'
					}
				},
				y2 : {
					label : {
						text : '',
						position : 'outer-middle'
					}
				},

				rotated : config.rotated,
			},

			grid : config.grid,
		// size : config.size
		});

		// Add border around chart
		$(".chart-zone").css("border", "1px solid black");
		$(".chart-options").css("border", "1px solid rgba(0, 0, 0, .1)");

		// Enable options for selected chart
		enableChartOptions(config.type);

		return true;
	} else
		return false;
}

/*
 * Export Chart config as string
 */

function exportChartConfig() {
	var result = {
		color : config.mycolor,
		type : config.type,
		label : config.label,
		keys : config.mykeys,
		cols : config.cols,
		data : config.json,
		grid : config.grid,
		subTypes : config.types,
		rotated : config.rotated,
		ticks : config.mytick,
	}

	return JSON.stringify(result);
}
/*
 * Displays the selected chart's options
 */
function enableChartOptions(type) {
	$("#chart-options p").text(type.toUpperCase() + " Options");
	$("#chart-options").show();

	if ($onlydata == true) {
		config.option = "data";
		$('.chart-options-menu span:contains(Data)').css({
			'background-color' : '#27a7d8',
			'color' : 'white'
		});
	}

	enableChartOptionForm();
	$('.chart-options-menu span:contains(' + config.option + ')').css({
		'background-color' : '#27a7d8',
		'color' : 'white'
	});

	if ($onlydata == true) {
		$(".chart-options-menu #dataspan").siblings().hide();
	}
}

/*
 * Display selected option form for chart
 */
function enableChartOptionForm() {
	if (config.option != null || config.option != "") {
		var ele = $("#" + config.option + "-option");
		ele.show(200);
		ele.siblings().hide(200);

		// data field
		if (config.option == "data") {
			// console.log("data");
			var htmlcode = "";
			config.keys
					.forEach(function(field) {
						htmlcode += "<label for='my"
								+ field
								+ "'> <input type='checkbox' name='data-fields' class='mycheckbox' style='padding:18px;"
								+ "' value='" + field + "' " + "id='my" + field
								+ "'>" + field + "</label>";
						// console.log(htmlcode);
						ele.find("#data-fields").html(htmlcode);
						ele.find(".data-fields").addClass("data-fields");
					});

			config.mykeys.forEach(function(e) {
				$('#my' + e).prop('checked', true);
			})
		}
		// display field
		if (config.option == "display") {
			forDisplay();
		}
		// ele.css({"width":"100%","height":"300px","overflow":"auto"});
	}
}

/*
 * forDisplay function
 */
function forDisplay() {

	var htmlcode = '';
	config.mykeys.forEach(function(e) {
		htmlcode += '<tr><td>' + e + '</td>' + '<td>' + '<input type="text" '
				+ 'id = ' + '"type-' + e + '"' + ' name ="type-' + e
				+ '"></td><tr>';
	})
	$('#type-properties').html(htmlcode);

	var htmlcode = '';
	config.mykeys.forEach(function(e) {
		htmlcode += '<tr><td>' + e + '</td>' + '<td>' + '<input type="text" '
				+ 'id = ' + '"' + e + '"' + ' name ="' + e + '"></td><tr>';

		console.log(htmlcode);
	})

	config.mykeys.forEach(function(e) {
		$('#' + e).colorpicker({
			hideButton : true
		});
	})

	$('#color-properties').html(htmlcode);
}
/*
 * END chart configuration ================================================
 */

/*
 * START WORKSPACE callbacks =============================================
 */

$(function() {

	// Publish chart to workspace
	$("#publish").click(function(e) {
		e.preventDefault();
		var data = {
			name : $("#chart_name").val(),
			workspace : $("#workspace").val(),
			json : exportChartConfig(),
			workspaceName : $("#workspace_name").val()
		};

		$.ajax({
			url : 'http://localhost:8080/HOBSAnalyticVisu/rest/workspace/save',
			type : 'POST',
			data : JSON.stringify(data),
			success : function(response, status) {
				console.log(response);
			},
			error : function(response, status) {
				console.log(repsonse);
				return false;
			}
		});
	});

	$('#workspace').change(function() {
		var myValue = $(this).val();

		if (myValue != '' && myValue === "create_new_workspace") {
			$("#newworkspace").show();
		} else {

			$("#newworkspace").hide();
		}
	});

	populateWorkspace();

});

function populateWorkspace() {
	$.ajax({
		url : 'http://localhost:8080/HOBSAnalyticVisu/rest/workspace/getDir',
		type : 'POST',
		success : function(response, status) {

			var workspaces = response.split(",");
			workspaces.forEach(function(e) {
				$("#workspace").append("<option>" + e + "</option>");
			});
		},
		error : function(err, status) {
			console.log(err);
		}
	});

}

/*
 * END OF WORKSPACE CALLBACK ===========================================
 */