function getValueFromTable() {
	var myRows = [];
	var $headers = $("th");
	var $rows = $("tr").each(function (index) {
		$cells = $(this).find("td");
		myRows[index] = {};
		$cells.each(function (cellIndex) {
			myRows[index][$($headers[cellIndex]).html()] = $(this).html();
		});
	});

	var h = getHeader($headers)

	var finalData = myRows.map((e)=>{
		return h.map(x=>{
			return e[x];
		})
	})

	console.log(finalData)
}

function getHeader($header){
	var $h = $header || $('tr#head th');
	var header = [];
	$h.each(function(index,value){
		header.push($($h[index]).html())
	})
	return header;
}

$(document).ready(function () {
	

	var header = getHeader();

	var filters = header.map(function(value){
		return {
			id:value.toLowerCase(),
			label:value,
			type: 'string'
		}
	});


	var options = {
		allow_empty: true,
		filters:filters

		// filters: [
		// {
		// id: 'name',
		// label: 'Name',
		// type: 'string',
		// // optgroup: 'core',
		// default_value: 'abc',
		// size: 30,
		// unique: true
		// },
		// {
		// id: 'category',
		// label: 'Category',
		// type: 'integer',
		// input: 'select',
		// values: {
		// 1: 'Books',
		// 2: 'Movies',
		// 3: 'Music',
		// 4: 'Tools',
		// 5: 'Goodies',
		// 6: 'Clothes'
		// },
		// operators: ['equal', 'not_equal', 'in', 'not_in', 'is_null',
		// 'is_not_null']
		// },
		// {
		// id: 'in_stock',
		// label: 'In stock',
		// type: 'integer',
		// input: 'radio',
		// values: {
		// 1: 'Yes',
		// 0: 'No'
		// },
		// operators: ['equal']
		// },
		// {
		// id: 'price',
		// label: 'Price',
		// type: 'double',
		// validation: {
		// min: 0,
		// step: 0.01
		// }
		// }]

	};

	$('#builder').queryBuilder(options);

	$('.parse-json').on('click', function () {
		alert(JSON.stringify(
			$('#builder').queryBuilder('getRules'),
			undefined, 2
		));
	});

	$('.btn-get-sql').on('click', function () {
		var result = $('#builder').queryBuilder('getSQL', 'question_mark');

		if (result.sql.length) {
			var sql_query = result.sql + '\n\n' + JSON.stringify(result.params, null, 2);
			$("#qry1").html(sql_query);
		}
	});

	$('.btn-reset').on('click', function () {
		$('#builder').queryBuilder('reset');
	});
});