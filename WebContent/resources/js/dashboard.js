

var chartList;

var form = $("#the-form");
form.validate({
	errorPlacement : function errorPlacement(error, element) {
		element.before(error);
	}
});
const swalCustomized = Swal.mixin({
	  customClass: {
	    confirmButton: 'btn btn-success w3-margin',
	    cancelButton: 'btn btn-danger w3-margin'
	  },
	  buttonsStyling: false
	});
	
$(function(){
	$('#grid1').gridstack({
		dragOut:false,
		animate:true,
		acceptWidgets: ".newChart",
		draggable: {
	        containment: $(".dashboard-container"),
	        scroll: true
	    },
	    resizable: {
	    	handles: 'se, sw',
	        containment: $(".dashboard-container"),
	        resize: function(e, ui) {
	            var contPos = $("#dialog-container").position();
	            contPos.bottom = contPos.top + $("#dialog-container").height();
	            contPos.right = contPos.left + $("#dialog-container").width();
	            contPos.height = $("#dialog-container").height();
	            contPos.width = $("#dialog-container").width();
	            if (ui.position.top <= contPos.top) {
	              ui.position.top = contPos.top + 1;
	            }
	            if (ui.position.left <= contPos.left) {
	              ui.position.left = contPos.left + 1;
	            }
	            if (ui.size.height >= contPos.height) {
	              ui.size.height = contPos.height - 7;
	            }
	            if (ui.size.width >= contPos.width) {
	              ui.size.width = contPos.width - 7;
	            }
	          }
	    },
	    added: function(e, items) {
	    	
	    	alert(items[0].innerHTML);
	    }
	});	
	$("#grid1").data("gridstack").removeAll();
	
	$('#grid2').gridstack({
		dragOut: true,
		animate:true,
		draggable: {
	        scroll: true
	    },
	    resizable:  {
	    	disabled: true
	    }
	    
	});	
});

$(function() {
	/* on added event of gridstack =============================================== */
	$("#grid1").on('added', function(e, items) {
		var x = e.target || e.srcElement;
		//alert( x.lastChild.innerHTML);
		
		theElement = x.lastChild;
		//var ch = $(".chart");
		//alert( $(theElement).prop("class") );
        //var newId = "chart" + ch.length;
        var newId = $(theElement).prop("class").split(" ")[0];
        //alert(newId);
      	
        theElement.setAttribute("id", "container_" + newId);
      	theElement.classList.add("chart");
        $(theElement).find('.grid-stack-item-content').html(' <p><input type="button" class="w3-btn w3-block w3-red btn-remove" value="Delete" onclick="deleteWidget(\'container_'+newId+'\')" /></p>     <div id="chart_' + newId + '" class="chart"> </div>  ');
        
		type = "line";
        //alert("new id : " + newId);
        getChartConfig(type,"chart_" + newId);
        
	});	/* END OF on added event of gridstack =============================================== */
	
	
	/*  on removed event of gridstack =============================================== */
	$('#grid1').on('removed', function(event, items) {
		var x = event.target || event.srcElement;
		//alert("onDelete \n \n" + x.innerHTML);		
		
	}); /* END OF on removed event of gridstack =============================================== */
	
	
});

function deleteWidget(element) {
	//el = $(element).closest('.grid-stack-item');
	//alert(el);
	//alert(el.innerHTML);
	//el = $(el);
	//alert($(el).html());
	//alert($(el).html());
	/* i = "#" + element;
	alert("***" + i + "****"); */
	swalCustomized.fire({
		  title: 'Are you sure?',
		  text: "You won't be able to revert this!",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonText: 'Yes, delete it!',
		  cancelButtonText: 'No, cancel!',
		  reverseButtons: true,
		  allowOutsideClick: false
		}).then((result) => {
		  
		  if (result.value) {
			  el = $("#"+element);
				el = $(el);
				//alert($(el).html());
				//alert($(el).prop("class").split(" ")[0]);
				var chartId = $(el).prop("class").split(" ")[0];
				if(chartList != null && chartList[chartId] != null && chartList[chartId][0] != null && chartList[chartId][0]["file"] != null){
					//alert(chartId + "\n" + chartList[chartId][0]["file"] + "\n" + chartList[chartId][0]["workspace"]  );
					if(chartList[chartId][0]["workspace"] == $("#workspace_list").val()){
						addChartInList(chartId, chartList[chartId][0]["file"]);
						}
				}
				else{
					//alert(chartList[chartId][0]["workspace"] + " is not from " + $("#workspace_list").val() );
				}
					
				$('#grid1').data('gridstack').removeWidget(el,true);
				swalCustomized.fire(
				      'Removed!',
				      'The Chart has been removed from dashboard and added back to the chart list.',
				      'success'
			    );
		  } else if (  result.dismiss === Swal.DismissReason.cancel )/* Read more about handling dismissals below */
		  {
		   /*  swalWithBootstrapButtons.fire(
		      'Cancelled',
		      'Your imaginary file is safe :)',
		      'error'
		    ); */
		  }
		});
}

$('#grid2').each(function () {
	$(this).resizable(".newChart", false)
});

function addChartInList(name, text){
	//alert("adding :" + name);
	$('#grid2').data('gridstack').addWidget( jQuery('<div id="list_'+name+'" class="'+name+' grid-stack-item newChart" data-gs-width="6" data-gs-height="2"> <div class="grid-stack-item-content"> '+text+' <div> </div>') );
}


async function loadGrid(){
	const { value: text } = await Swal.fire({
		  input: 'textarea',
		  inputPlaceholder: 'Type your message here...',
		  inputAttributes: {
		    'aria-label': 'Type your message here'
		  },
		  showCancelButton: true
		})

		loadDashboard(text);		
}

function loadDashboard(grid){
	//showLoader();
	if (grid) {			
		var data = grid;
		//alert(data);
	  //Swal.fire(text)
		var obj = JSON.parse(data.trim());
	  	//alert(obj[0]);
		var items = GridStackUI.Utils.sort(obj );
		//serialization = GridStackUI.Utils.sort(serialization);
		$('#grid1').data('gridstack').removeAll();

	   /*  $.each( items, function( node ) {
	    	alert(node['width'] + ", " + node['height'] );
	    	$('#grid1').data('gridstack').addWidget( jQuery( '<div class="'+node.id+' grid-stack-item"><div class="grid-stack-item-content"> <p><input type="button" class="w3-btn w3-block w3-red btn-remove" value="Delete" onclick="deleteWidget(\'container_'+node.id+'\')" /></p>     <div id="' + node.id + '" class="chart"> </div>  </div></div>' ), node.x, node.y, node.width, node.height );
	    }, this );
	     */
	    items.forEach(function (node) {
            //$("#grid1").data("gridstack").addWidget($('<div><div class="grid-stack-item-content"></div></div>'), node);
	    	$("#grid1").data("gridstack").addWidget($('<div class="'+node.id+' grid-stack-item"><div class="grid-stack-item-content"> <p><input type="button" class="w3-btn w3-block w3-red btn-remove" value="Delete" onclick="deleteWidget(\'container_'+node.id+'\')" /></p>     <div id="' + node.id + '" class="chart"> </div>  </div></div>'), node);
	    	el = $("#list_"+node.id);		    	
	    	try {
	    		$('#grid2').data('gridstack').removeWidget(el);
	    	}
	    	catch(err) {
	    		  
	    	}
	    	
          }, this);
	  //  hideLoader();
	}
	//hideLoader();
}

async function loadGrid2(){
	const { value: text } = await Swal.fire({
		  input: 'textarea',
		  inputPlaceholder: 'Type your message here...',
		  inputAttributes: {
		    'aria-label': 'Type your message here'
		  },
		  showCancelButton: true
		})

		loadDashboard2(text);		
}

function loadDashboard2(grid){
	//showLoader();
	if (grid) {			
		var data = grid;
		//alert(data);
	  //Swal.fire(text)
		var obj = JSON.parse(data.trim());
	  	//alert(obj[0]);
		var items = GridStackUI.Utils.sort(obj );
		//serialization = GridStackUI.Utils.sort(serialization);
		$('.grid-stack').data('gridstack').removeAll();

	   /*  $.each( items, function( node ) {
	    	alert(node['width'] + ", " + node['height'] );
	    	$('#grid1').data('gridstack').addWidget( jQuery( '<div class="'+node.id+' grid-stack-item"><div class="grid-stack-item-content"> <p><input type="button" class="w3-btn w3-block w3-red btn-remove" value="Delete" onclick="deleteWidget(\'container_'+node.id+'\')" /></p>     <div id="' + node.id + '" class="chart"> </div>  </div></div>' ), node.x, node.y, node.width, node.height );
	    }, this );
	     */
	    items.forEach(function (node) {
            //$("#grid1").data("gridstack").addWidget($('<div><div class="grid-stack-item-content"></div></div>'), node);
	    	$(".grid-stack").data("gridstack").addWidget($('<div class="'+node.id+' grid-stack-item "><div class="grid-stack-item-content">    <div id="' + node.id + '" class="chart"> </div>  </div></div>'), node);
	    	el = $("#list_"+node.id);		    	
	    	
          }, this);
	  //  hideLoader();
	}
	//hideLoader();
}

function saveData() {
	var items = [];

    $('#grid1 .grid-stack-item.ui-draggable').each(function () {
        var $this = $(this);
        items.push({
            'x': $this.attr('data-gs-x'),
            'y': $this.attr('data-gs-y'),
            'width': $this.attr('data-gs-width'),
            'height': $this.attr('data-gs-height'),
            'id': $this.attr("class").split(" ")[0]
            //content: $('.grid-stack-item-content', $this).html()
        });        
    });

    //alert(JSON.stringify(items));
    //$("#txtGrid").val(JSON.stringify(items));
   // form = $("#the-form");
	//form.validate().settings.ignore = ":disabled,:hidden";
	//alert(escape( JSON.stringify(items)));
	
	if( JSON.stringify(items).length < 3){
		swalCustomized.fire({
			 title:'Dashboard EMPTY!',
		      text:'Please Add Chart(s) to the Dashboard',
		      icon:'error'			     
		});
		return;
	}
	var myexpr = /^([a-zA-Z0-9 _-]){3,}$/;
	var val = $("#dashboard_name").val();
	//alert(myexpr.test(val));
    if(! myexpr.test(val) ){
    	swalCustomized.fire({
			 title:'Error!',
		      text:'Please Enter the Dashboard Name',
		      icon:'error'     
		}).then((result) => {
			$("#dashboard_name").focus();	  
		});
    	return;
    }
    
    var exp = /^[0-9]{1,}$/;
    if( exp.test($("#hf_dashboard_id").val()) )
    {
    	updateDashboard();
    }
    else{
    	saveDashboard();
    }
   
} 
/*  END OF  saveData function ============================================= */


function saveDashboard(){
	var items = [];

    $('#grid1 .grid-stack-item.ui-draggable').each(function () {
        var $this = $(this);
        items.push({
            'x': $this.attr('data-gs-x'),
            'y': $this.attr('data-gs-y'),
            'width': $this.attr('data-gs-width'),
            'height': $this.attr('data-gs-height'),
            'id': $this.attr("class").split(" ")[0]
            //content: $('.grid-stack-item-content', $this).html()
        });        
    });
    	showLoader();
    	$.ajax({
			url : 'http://localhost:8080/HOBSAnalyticVisu/rest/dashboard/create/' + $("#dashboard_name").val() + "/"+ escape( JSON.stringify(items)) ,
			type : 'GET',
			dataType : 'JSON',
			success : function(response, status) {
				
				hideLoader();
				if (status == "success") {
					$("#hf_dashboard_id").val(response);
					$("#hf_dashboard_state").val( JSON.stringify(items));
					$("#hf_dashboard_name").val( $("#dashboard_name").val() );
					
					swalCustomized.fire(
						      'Success!',
						      'Dashboard Configuration has been saved successully' ,
						      'success'
						    );
				} else {
					swalCustomized.fire(
						      'Error!',
						      'Could not be able to save the Dashboard Configuration',
						      'error'
						    );
				}
			},
			error : function(response, status) {
				hideLoader();
				swalCustomized.fire({
					      title:'Error!',
					      text:'Could not be able to save the Dashboard Configuration',
					      icon:'error'
				});
			}
		});
   
} /*  END OF  saveDashboard function ============================================= */

function updateDashboard(){
	var items = [];

    $('#grid1 .grid-stack-item.ui-draggable').each(function () {
        var $this = $(this);
        items.push({
            'x': $this.attr('data-gs-x'),
            'y': $this.attr('data-gs-y'),
            'width': $this.attr('data-gs-width'),
            'height': $this.attr('data-gs-height'),
            'id': $this.attr("class").split(" ")[0]
            //content: $('.grid-stack-item-content', $this).html()
        });        
    });
    
	var exp = /^[0-9]{1,}$/;
    if( exp.test($("#hf_dashboard_id").val()) )
    {
    	if( $("#dashboard_name").val() == $("#hf_dashboard_name").val()  && JSON.stringify(items) ==  $("#hf_dashboard_state").val() ){
    		swalCustomized.fire(
				      'Yola!',
				      'Dashboard Configuration already saved' ,
				      'success'
				    );
    	}
    	else if( $("#dashboard_name").val() == $("#hf_dashboard_name").val()  && JSON.stringify(items) != $("#hf_dashboard_state").val() ){
    		doUpdate();
    	}
    	else if( $("#dashboard_name").val() != $("#hf_dashboard_name").val()){
    		saveDashboard();
    	}	
    	
    	    	 
    }
}  /*  END OF  updateDashboard function ============================================= */

function doUpdate(){
	showLoader();
	
	var items = [];

    $('#grid1 .grid-stack-item.ui-draggable').each(function () {
        var $this = $(this);
        items.push({
            'x': $this.attr('data-gs-x'),
            'y': $this.attr('data-gs-y'),
            'width': $this.attr('data-gs-width'),
            'height': $this.attr('data-gs-height'),
            'id': $this.attr("class").split(" ")[0]
            //content: $('.grid-stack-item-content', $this).html()
        });
    });
   	
   	$.ajax({
		url : 'http://localhost:8080/HOBSAnalyticVisu/rest/dashboard/update/' + $("#hf_dashboard_id").val() + "/" + $("#dashboard_name").val()	 + "/"+ escape( JSON.stringify(items)) ,
		type : 'GET',
		dataType : 'JSON',
		success : function(response, status) {
			//$("#hf_dashboard_id").val(response);
			$("#hf_dashboard_state").val( JSON.stringify(items));
			$("#hf_dashboard_name").val( $("#dashboard_name").val() );
			
			hideLoader();
			if (status == "success") {				
				swalCustomized.fire(
					      'Success!',
					      'updated Dashboard Configuration has been saved successully' ,
					      'success'
					    );
			} else {
				swalCustomized.fire(
					      'Oops!',
					      'Could not be able to update the Dashboard Configuration',
					      'error'
					    );
			}
		},
		error : function(response, status) {
			hideLoader();
			swalCustomized.fire({
			      title:'Error!',
			      text:'Could not be able to save the Dashboard Configuration',
			      icon:'error'
			});
		}
	});
}  /*  END OF  doUpdate function ============================================= */

function showLoader(){
	document.getElementById("customLoader").classList.add("is-active");
}
function hideLoader(){
	document.getElementById("customLoader").classList.remove("is-active");
}

function loadWorkspaces()
{
	showLoader();
	$.ajax({
		url : 'http://localhost:8080/HOBSAnalyticVisu/rest/dashboard/workspaces/get' ,
		type : 'GET',
		dataType : 'JSON',
		success : function(response, status) {			
			
			if (status == "success") {
				if(response[0] == "error"){
					swalCustomized.fire(
						      'Oops!',
						      'Could not be able to load workspaces',
						      'error'
						    );
				}
				else if(response[0] == "empty"){
					swalCustomized.fire(
						      'Oh Crap!',
						      '<b>' + resose[1] +  '</b> worksace is empty!',
						      'waring'
						    );
				}
				else if(response[0] == "success"){
					
				}
				let dropdown = $('#workspace_list');
				dropdown.empty();
				dropdown.append('<option selected="true" disabled>Select a Workspace</option>');
				dropdown.prop('selectedIndex', 0);
				
				for(i=1, l=response.length; i <l; i++){
					dropdown.append($('<option></option>').attr(	'value',	response[i]).text(response[i]));
				}
				hideLoader();
				/* swalCustomized.fire(
					      'Success!',
					      'updated Dashboard Configuration has been saved successully' ,
					      'success'
					    ) */;
			} else {
				hideLoader();
				swalCustomized.fire(
					      'Oops!',
					      'Could not be able to update workspaces',
					      'error'
					    );
			}
		},
		error : function(response, status) {
			hideLoader();
			swalCustomized.fire({
			      title:'Error!',
			      text:'Could not be able to load Workspaces',
			      icon:'error'
			});
		}
	});
	
} /* END OF loadWorkspaces()  =========================================================== */


/*  retriveDashboard()  =========================================================== */
function retriveDashboard(){
	showLoader();
	var urlParams = new URLSearchParams(location.search);
	//alert(urlParams.has('dashboard'));
	if(urlParams.has('dashboard')){
		var dashboardId = urlParams.get('dashboard');
		var exp = /^[0-9]{1,}$/;
		if(! exp.test(dashboardId)){
			//hideLoader();
			loadWorkspaces();
			swalCustomized.fire({
			      title:'Error!',
			      text:'invalid dashboard id',
			      icon:'error'
			});			
			return;
		}
			$.ajax({
				url : 'http://localhost:8080/HOBSAnalyticVisu/rest/dashboard/get/'  + dashboardId,
				type : 'GET', 
				dataType : 'JSON',
				success : function(response, status) {	
					if(response[0] == "success"){
						//alert(response[0]);
						//alert(response[1] + "\n" + response[2] + "\n" + response[3] );
						loadDashboard(response[3]);
						$("#hf_dashboard_id").val(response[1]);
						$("#hf_dashboard_state").val( JSON.stringify(response[3] ) );
						$("#hf_dashboard_name").val( response[2] );
						$("#dashboard_name").val( response[2] );
						
						hideLoader();
					}
					else if(response[0] == "not found"){
						swalCustomized.fire({
						      title:'Oops!',
						      text:'Dashboard not found',
						      icon:'error'
						});
					}
					else if(response[0] == "exception"){
						swalCustomized.fire({
						      title:'Error!',
						      text:'Exception occured while loading Dashboard',
						      icon:'error'
						});
					}
					loadWorkspaces();
				},
				error : function(response, status) {					
					hideLoader();
					swalCustomized.fire({
					      title:'Error!',
					      text:'Could not be able to load the Dashboard',
					      icon:'error'
					});
					loadWorkspaces();
				}			
			});
			//hideLoader();				
	}	
	else{
		loadWorkspaces();
	}
}
/* END OF retriveDashboard()  =========================================================== */

$(document).ready(function(){	
	$("#customLoader").removeClass("is-active");
	retriveDashboard();	
	
	/* get chart list of a from selected workspace  =========================================================== */
	$("#workspace_list").change(function(){
		chartList = null;
		//alert($("#workspace_list").val());
		showLoader();	
		$.ajax({
			url : 'http://localhost:8080/HOBSAnalyticVisu/rest/dashboard/charts/get/'  + $("#workspace_list").val(),
			type : 'GET', 
			dataType : 'JSON',
			success : function(response, status) {			
				
				if (status == "success") {
					$("#grid2").data("gridstack").removeAll();
					if(response[0] == "error"){
						hideLoader();
						swalCustomized.fire(
							      'Oops!',
							      'error occured while loading charts',
							      'error'
							    );
					}
					else if(response[0] == "empty"){
						hideLoader();
						swalCustomized.fire(
								  'Oh Crap!',
							      '<b>' + response[1] +  '</b> workspace is empty!',
							      'warning'
							    );
					}
					else if(response[0] == "success"){
						chartList = response[1];
						//alert(JSON.stringify(chartList));
						
						var items = JSON.parse(chartList.trim());
						
						var a = [];						
						for( j in items.charts){							
							a[j] = [];
						}						
						var keys = Object.keys(items.charts);
					    keys.forEach(function(key){
					    	a[key] = items.charts[key];
					    	if(! $("#grid1 ." + key).length)
					    		addChartInList(key, a[key][0]["file"]);
					    });
					    chartList = a;					    
					   // alert(chartList[3777016][0]["file"]);
					    
					}					
					hideLoader();					
				} else {
					hideLoader();
					swalCustomized.fire(
						      'Oops!',
						      'Could not be able to load charts',
						      'error'
						    );
				}
			},
			error : function(response, status) {
				$("#grid2").data("gridstack").removeAll();
				hideLoader();
				swalCustomized.fire({
				      title:'Error!',
				      text:'Could not be able to load charts',
				      icon:'error'
				});
			}
		});
		
		//hideLoader();
	});
	/* END OF get chart list of a from selected workspace  =============================================== */

});


